<?php
/**
 * @author  RadiusTheme
 * @since   1.3.4
 * @version 1.3.4
 */

namespace radiustheme\Classima;

if ( function_exists( 'elementor_theme_do_location' ) && elementor_theme_do_location( 'footer' ) ) {
	return;
}

$footer_columns = 0;

foreach ( range( 1, 4 ) as $i ) {
	if ( is_active_sidebar( 'footer-'. $i ) ){
		$footer_columns++;
	}
}

switch ( $footer_columns ) {
	case '1':
	$footer_class = 'col-sm-12 col-12';
	break;
	case '2':
	$footer_class = 'col-sm-6 col-12';
	break;
	case '3':
	$footer_class = 'col-md-4 col-sm-12 col-12';
	break;
	default:
	$footer_class = 'col-lg-3 col-sm-6 col-12';
	break;
}
$copyright_class = RDTheme::$options['payment_icons'] ? 'col-md-8 col-sm-12 col-12' : 'col-sm-12 col-12 text-center';
?> 

<footer>
<!-- footer menu -->
 <div class="footer-top-areab">
<div class="container">
<div class="row">
    <div class="col-md-4">
<p><i class="fa fa-user-plus ikonfooter" aria-hidden="true"></i> <a class="fmenu" href="https://www.oplatim.cz/muj-ucet/" alt="Založit účet"><b>Založit účet</b></a><br>
<i class="fa fa-cogs ikonfooter" aria-hidden="true"></i> <a class="fmenu" href="https://www.oplatim.cz/jak-to-funguje/" alt="Jak to funguje">Jak to funguje</a><br>
<i class="fa fa-user-slash ikonfooter" aria-hidden="true"></i> <a class="fmenu" href="https://www.oplatim.cz/odstraneni-sveho-uctu-na-oplatim-cz/" alt="Odstranění svého účtu">Odstranění svého účtu</a></p>
    </div>
   <div class="col-md-4 fico">
<p><i class="fa fa-exclamation-triangle ikonfooter" aria-hidden="true"></i> <a class="fmenu" href="https://www.oplatim.cz/pravidla-inzerce/" alt="Pravidla inzerce"><b>Pravidla inzerce</b></a><br>
<i class="fa fa-book ikonfooter" aria-hidden="true"></i> <a class="fmenu" href="https://www.oplatim.cz/ochrana-osobnich-udaju/" alt="Zásady ochrany osobních údajů">Zásady ochrany osobních údajů</a><br>
<i class="fa fa-book ikonfooter" aria-hidden="true"></i> <a class="fmenu" href="https://www.oplatim.cz/smenna-smlouva/" alt="Směnná smlouva">Směnná smlouva</a></p>
    </div>
    <div class="col-md-4">
<p><i class="fa fa-user-plus ikonfooter" aria-hidden="true"></i> <a class="fmenu" href="https://www.oplatim.cz/kontak/" alt="Kontakt">Kontakt</a><br>    
<i class="fa fa-envelope-open-text ikonfooter" aria-hidden="true"></i> <a class="fmenu" href="https://www.facebook.com/oplatimcz" alt="Oplatim.cz na facebooku" target="_blank">Facebook</a></p>
    </div>

 </div> 
</div>
 
 
 
</div> 

<!-- copy -->
<div class="footer-top-area">


			<div class="container">
      
    <div class="row">
        <div class="col-lg-6 mob leva">
<i class="fab fa-cc-visa" aria-hidden="true" id="ikonypay"></i> <i class="fab fa-cc-stripe" aria-hidden="true" id="ikonypay"></i> <i class="fab fa-cc-paypal" aria-hidden="true" id="ikonypay"></i> <i class="fab fa-cc-mastercard" aria-hidden="true" id="ikonypay"></i> <i class="fab fa-cc-amex" aria-hidden="true" id="ikonypay"></i> <i class="fab fa-cc-discover" aria-hidden="true" id="ikonypay"></i>        
        </div>
        
        <div class="col-lg-6 mobb prava footerlogo">
<a href="https://www.oplatim.cz/"><img title="Oplatim.cz" src="https://www.oplatim.cz/wp-content/uploads/2021/11/logo-footer.png" alt="Oplatim.cz" width="130" height="33" /></a>
        </div>                        
    </div>
    

        	<div class="row">
        
<div class="col medium-12 small-12 large-12 mob footerpay leva">
 <p>2020 – <?php echo date('Y'); ?> © Copyright <a href="https://www.oplatim.cz/" alt="Největší databáze protislužeb a bárterových obchodů."><strong>Oplatim.cz</strong></a> | Největší databáze protislužeb a bárterových obchodů.</p>

</div>

      </div> 
          



               </div>
 </div>     
</footer>