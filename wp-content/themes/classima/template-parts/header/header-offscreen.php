<?php
/**
 * @author  RadiusTheme
 * @since   1.0
 * @version 1.0
 */

namespace radiustheme\Classima;

use Rtcl\Helpers\Link;

$nav_menu_args   = Helper::nav_menu_args();

$logo = empty( RDTheme::$options['logo']['url'] ) ? Helper::get_img( 'logo-dark.png' ) : RDTheme::$options['logo'];

if (!empty($logo['url'])) {
    $rdtheme_logo = '<img class="logo-small" src="'. esc_url( $logo['url'] ).'" width="'. esc_attr( $logo['width'] ).'" height="'. esc_attr( $logo['height'] ).'" />';
} else {
    $rdtheme_logo = '<img class="logo-small" src="'. esc_url( $logo ).'" width="150" height="45" />';
}
?>

<div class="rt-header-menu mean-container" id="meanmenu">
    <div class="mean-bar">
    	<a class="mean-logo-area" href="<?php echo esc_url(home_url('/'));?>" alt="<?php echo esc_attr( get_bloginfo( 'title' ) );?>"><?php echo wp_kses_post($rdtheme_logo);?></a>
        <?php
        $html = '';

        if ( RDTheme::$options['header_btn_txt_mob'] && RDTheme::$options['header_btn_url'] ) {
            $html .= '<a class="header-btn header-menu-btn header-menu-btn-mob" href="'.esc_url( RDTheme::$options['header_btn_url'] ).'"><i class="fas fa-plus" aria-hidden="true"></i><span>'.esc_html( RDTheme::$options['header_btn_txt_mob'] ).'</span></a>';
        }

        if ( Helper::is_chat_enabled() ) {
            $html .= '<a class="header-chat-icon header-chat-icon-mobile rtcl-chat-unread-count" href="'.esc_url( Link::get_my_account_page_link( 'chat' ) ).'"><i class="far fa-comments" aria-hidden="true"></i></a>';
        }

        if ( RDTheme::$options['header_icon'] && class_exists( 'RtclPro' ) && class_exists( 'Rtcl' ) ) {
            $html .= '<a class="header-login-icon header-login-icon-mobile" href="'.esc_url( Link::get_my_account_page_link() ).'"><i class="far fa-user" aria-hidden="true"></i></a>';
        }

        if ( $html ) {
            printf( '<div class="header-mobile-icons">%s</div>' , $html );
        }
        ?>
        <span class="sidebarBtn ">
            <span></span>
        </span>

    </div>
    <div class="rt-slide-nav">
        <div class="offscreen-navigation">
            <?php wp_nav_menu( $nav_menu_args ); ?>
        </div>
    </div>
</div>