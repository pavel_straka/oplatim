<?php
/**
 * @author  RadiusTheme
 * @since   1.0
 * @version 1.0
 */

namespace radiustheme\Classima;

$has_top_info = RDTheme::$options['address'] || RDTheme::$options['phone'] || RDTheme::$options['email'] ? true : false;
$socials = Helper::socials();

if ( !$has_top_info && !$socials ) {
	return;
}
?>
<div class="top-header">
	<div class="container">
		<div class="top-header-inner">
				<div class="tophead-left">
					<ul class="tophead-info">
							<li><i class="fa fa-user-plus" aria-hidden="true"></i><a href="https://www.oplatim.cz/muj-ucet/" alt="Založit účet - Registrace">Registrovat se</a></li>
                          	<li><i class="fa fa-question-circle" aria-hidden="true"></i><a href="https://www.oplatim.cz/jak-to-funguje/" alt="Jak to funguje">Jak to funguje</a></li>
				            <li><a href="https://www.facebook.com/oplatimcz" alt="Facebook" target="_blank"><i class="fab fa-facebook-f ikona" aria-hidden="true"></i> Facebook</a></li> 

                	</ul>
				</div>

				<div class="tophead-right">
                    <ul class="tophead-info">  
                <li>  
<span class="duser"><?php global $current_user;
      get_currentuserinfo(); 
      echo '' . $current_user->user_firstname . " "; echo '' . $current_user->user_lastname . "   ";
      echo '<span class="ustop">' . $current_user->user_login . "</span> ";

?>  </span>

</li>

<li><?php if ( is_user_logged_in() ) { ?>
   <a href="<?php echo wp_logout_url(); ?>" class="ology">Odhlášení</a>
<?php } else { ?>
    <a href="https://www.oplatim.cz/muj-ucet/" title="Přihlášení" rel="home" class="logy">Přihlásit se</a>
<?php } ?></li>

                 	</ul>
				</div>

		</div>
	</div>
</div>

