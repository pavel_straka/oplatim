<?php
/**
 * @author  RadiusTheme
 * @since   1.0
 * @version 1.5.4
 */

if ( !isset( $content_width ) ) {
	$content_width = 1240;
}

class Classima_Main {

	public $theme   = 'classima';
	public $action  = 'classima_theme_init';

	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'load_textdomain' ) );
		remove_action( 'admin_notices',     array( $this, 'plugin_update_notices' ) );
		$this->includes();
	}

	public function load_textdomain(){
		load_theme_textdomain( $this->theme, get_template_directory() . '/languages' );
	}

	public function includes(){
		require_once get_template_directory() . '/inc/constants.php';
		require_once get_template_directory() . '/inc/helper.php';
		require_once get_template_directory() . '/inc/includes.php';

		do_action( $this->action );
	}

	public function plugin_update_notices() {
		$plugins = array();

		if ( defined( 'CLASSIMA_CORE' ) ) {
			if ( version_compare( CLASSIMA_CORE, '1.5', '<' ) ) {
				$plugins[] = 'Classima Core';
			}
		}

		if ( defined( 'RTCL_VERSION' ) ) {
			if ( version_compare( RTCL_VERSION, '1.5.55', '<' ) ) {
				$plugins[] = 'Classified Listing Pro';
			}
		}

		if ( defined( 'RTCL_STORE_VERSION' ) ) {
			if ( version_compare( RTCL_STORE_VERSION, '1.3.20', '<' ) ) {
				$plugins[] = 'Classified Listing Store';
			}
		}

		foreach ( $plugins as $plugin ) {
			$notice = '<div class="error"><p>' . sprintf( __( "Please update plugin <b><i>%s</b></i> to the latest version otherwise some functionalities will not work properly. You can update it from <a href='%s'>here</a>", 'classima' ), $plugin, menu_page_url( 'classima-install-plugins', false ) ) . '</p></div>';
			echo wp_kses_post( $notice );
		}
	}
}

new Classima_Main;
/* .puvodní  */
add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );


// hide update notifications
function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates'); //hide updates for WordPress itself
add_filter('pre_site_transient_update_plugins','remove_core_updates'); //hide updates for all plugins
add_filter('pre_site_transient_update_themes','remove_core_updates'); //hide updates for all themes

/* vlastní admin stránka - Možnosti */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Možnosti',
		'menu_title'	=> 'Možnosti webu',
		'menu_slug' 	=> 'noznosti-webu',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	
}
/* odebere komentáře */
function hide_comments_div() {
global $pagenow;
if ($pagenow=='post-new.php' OR $pagenow=='post.php')
        echo '<style>#commentstatusdiv{ display:none; }</style>';
}
add_action('admin_head', 'hide_comments_div');


/* url pro odhlášení - v TOP */
add_action( 'wp_logout','wpdocs_ahir_redirect_after_logout' );
function wpdocs_ahir_redirect_after_logout() {
    wp_safe_redirect( home_url('https://www.oplatim.cz/muj-ucet/') );
    exit();
}

function misha_my_load_more_scripts() {
	wp_enqueue_script('jquery');
	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/assets/js/set-profile-image.js', array('jquery') );
	/*wp_localize_script( 'my_loadmore', 'update_profile', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php' // WordPress AJAX

	) );*/
 
 	wp_enqueue_script( 'my_loadmore' );
}
 
add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );
add_action( 'admin_enqueue_scripts', 'misha_my_load_more_scripts' );

add_action('wp_ajax_updateprofile', 'custom_update_profile'); 
add_action('wp_ajax_nopriv_updateprofile', 'custom_update_profile');

 
 
function custom_update_profile(){
    $idImage = $_POST['imageID'];
    if ( is_user_logged_in() ) {
        $exist = get_user_meta(get_current_user_id(),'_rtcl_pp_id',true);
        $args = ['post_type' => 'store', 'post_author' => get_current_user_id()];
        $store = get_posts($args);

        if($exist || $exist == NULL){
            update_user_meta(get_current_user_id(),'_rtcl_pp_id',$idImage);
            update_post_meta($store[0]->ID,'logo_id',$idImage);
        }
        else{
            add_user_meta(get_current_user_id(),'_rtcl_pp_id',$idImage); 
            add_post_meta($store[0]->ID,'logo_id',$idImage);           
        }
    }
    $response = array('message'=>'Fotografie byla nastavena');
    wp_send_json( $response );
    die;
}


function text_logout_user(){
    if (is_user_logged_in()){
        $con =  '';   
    }
    else{
        $con = 'Pro vložení inzerátu je nutné být <strong>přihlášen</strong>. Pokud ještě nemáte účet, můžete jej snadno vytvořit pomocí registrace <a href="https://www.oplatim.cz/muj-ucet/"><strong>zde</strong></a>.'; 
    }
    return $con;
}
add_shortcode('registration_text','text_logout_user');
//add_action('init', 'cu{stom_update_post' );


// stav uživatele - online-offline 
function is_user_online( $user_id ) {
    // get the online users list
    $logged_in_users = get_transient( 'users_online' );

    // online, if (s)he is in the list and last activity was less than 15   minutes ago
    return isset( $logged_in_users[$user_id] ) && ( $logged_in_users[$user_id] >     ( current_time( 'timestamp' ) - ( 15 * 60 ) ) );   
}
// .stav uživatele - online-offline
