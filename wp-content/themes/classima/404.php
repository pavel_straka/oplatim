<?php
/**
 * @author  RadiusTheme
 * @since   1.0
 * @version 1.0
 */

namespace radiustheme\Classima;

$rdtheme_error_img = empty( RDTheme::$options['error_bodybanner']['url'] ) ? Helper::get_img( '404.png' ) : RDTheme::$options['error_bodybanner']['url'];
?>
<?php get_header();?>
<div id="primary" class="content-area">
	<div class="container" style="min-height: 400px;">
		<div class="error-page">
       
		<div class="row">        

 <div class="col-lg-6 col-md-6 col-sm-6 col-12">
<img id="imgmob" src="<?php echo esc_url( $rdtheme_error_img );?>" alt="<?php esc_attr_e( '404', 'classima' );?>">
</div>       

<div class="col-lg-6 col-md-6 col-sm-6 col-12" style="margin-bottom: 58px;">         
			<h3><?php echo esc_html( RDTheme::$options['error_text'] );?></h3>  
                        <p>Nuda co? Je nám líto, ale stránku, kterou hledáte, není možné najít.<br>Byla smazána, přesunuta nebo se prostě jen vypařila.
                        <br><b>Máme ale lék!</b> Stačí přejít na domovskou stránku a inzerentit dál</p>
			<a class="error-btn" href="<?php echo esc_url( home_url( '/' ) );?>"><?php echo esc_html( RDTheme::$options['error_buttontext'] );?></a>  
</div>  






 </div>
        
        
			
   
		</div>
	</div>
</div>
<?php get_footer(); ?>