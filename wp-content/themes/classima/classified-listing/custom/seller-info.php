<?php
/**
 * @author  RadiusTheme
 * @since   1.0
 * @version 1.4
 */

namespace radiustheme\Classima;

use Rtcl\Models\Listing;
use Rtcl\Helpers\Link;
use Rtcl\Helpers\Functions;
use RtclPro\Helpers\Fns;
use RtclStore\Helpers\Functions as StoreFunctions;

$id           = get_the_id();
$listing      = new Listing( $id );
$listing_post = $listing->get_listing();
$email        = get_post_meta( $id, 'email', true );
$website      = get_post_meta( $id, 'website', true );
$phone        = get_post_meta( $id, 'phone', true );
$whatsapp     = get_post_meta( $id, '_rtcl_whatsapp_number', true );

$has_contact_form        = Functions::get_option_item( 'rtcl_moderation_settings', 'has_contact_form', false, 'checkbox');
$alternate_contact_form  = Functions::get_option_item( 'rtcl_moderation_settings', 'alternate_contact_form_shortcode');

$store        = false;
if ( class_exists( 'RtclStore' ) ) {
	$store = StoreFunctions::get_user_store( $listing_post->post_author );
}
?>
<div class="classified-seller-info widget">


        
        
<!-- další info  -->
	<div class="rtin-box">
    
 <!-- foto a jméno autora  -->
        <?php if ( $listing->can_show_user() ): ?>
            <div class="rtin-author">
                <?php if ( $store ): ?>
                    <?php $store->the_logo();?>
                    <h2 class="rtin-name"><?php $listing->the_author();?></h2>
                <?php else: ?>
                  <h3 class="rtin-name"><?php $listing->the_author(); ?></h3>
                <?php endif; ?>
            </div>
        <?php endif; ?>   
    


        <?php if (Fns::registered_user_only('listing_seller_information') && !is_user_logged_in()) { ?>
            <p class="login-message stred"><?php echo wp_kses(sprintf(__("Please <a href='%s'>login</a> to view the seller information.", "classima"), esc_url(Link::get_my_account_page_link())), ['a' => ['href' => []]]); ?></p>
        <?php } else { ?>
        

           <div class="linka"> </div>
<!-- telefon --> 
            <?php if ( $phone || $whatsapp): ?>
                <div class="rtin-phone"><?php Listing_Functions::the_phone( $phone, $whatsapp );?></div>
            <?php endif; ?> 
<!-- .telefon -->
             


<!-- odkaz na profil -->
<a href="<?php $store->the_permalink();?>"><div class="rtin-price"><i class="fas fa-user ikonab" aria-hidden="true"></i> Zobrazit profil</div></a> 
<!-- .odkaz na profil -->

            <?php
            if (Fns::is_enable_chat() && ((is_user_logged_in() && $listing->get_author_id() !== get_current_user_id()) || !is_user_logged_in())):
                $chat_btn_class = ['rtcl-chat-link'];
                $chat_url = Link::get_my_account_page_link();
                if (is_user_logged_in()) {
                    $chat_url = '#';
                    array_push($chat_btn_class, 'rtcl-contact-seller');
                } else {
                    array_push($chat_btn_class, 'rtcl-no-contact-seller');
                }
                ?>   
                <div class="media rtin-chat kontbox">
                    <a class="<?php echo esc_attr(implode(' ', $chat_btn_class)); ?>" data-listing_id="<?php the_ID(); ?>" href="<?php echo esc_url($chat_url) ?>"><i class="fa fa-comments" aria-hidden="true"></i>Chat</a>
                </div> 
            <?php endif; ?>
 
            <?php if ( $has_contact_form && ( $email || $alternate_contact_form ) ) : ?>
                <div class="media rtin-email kontbox">
                    <a data-toggle="modal" data-target="#classima-mail-to-seller" href="#"><i class="fas fa-envelope" aria-hidden="true"></i><?php esc_html_e( 'Poslat e-mail' );?></a>
                </div>
              
            <?php endif; ?>
        <?php } ?>  

	</div>
</div>