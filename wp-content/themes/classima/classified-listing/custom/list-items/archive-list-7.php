<?php
/**
 * Seznam inzerátů slyt: List
 */

namespace radiustheme\Classima;

use Rtcl\Helpers\Link;
use Rtcl\Helpers\Functions;
use RtclPro\Controllers\Hooks\TemplateHooks;
use RtclPro\Helpers\Fns;

$phone = get_post_meta( $listing_post->ID, 'phone', true );
?>
<div class="listing-list-each listing-list-each-6<?php echo esc_attr( $class ); ?>">
	<div class="rtin-item">
        <div class="rtin-thumb catobr">
            <a class="rtin-thumb-inner rtcl-media" href="<?php the_permalink(); ?>"><div class="catobr2"><?php $listing->the_thumbnail(); ?></div></a>
            <?php TemplateHooks::sold_out_banner(); ?>
        </div>
    
        
        
		<div class="rtin-content-area inz-grid-sub"> 
			<div class="rtin-content">                                                       

		<?php
				if ( $display['label'] ) {
					$listing->the_badges();
				}
				?> <br>
<!-- nadpis -->
				<h3 class="rtin-title listing-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<!-- .nadpis -->

<!-- obsah -->
<ul class="rtin-meta">

<!-- autor -->
<?php
$authorID = get_the_author_meta( 'ID' );
$args = ['post_type' => 'store', 'author' => $authorID, 'numberposts' => 1];
$post = get_posts($args);
$linkProfile = $post[0]->post_name;
?>
<li class="rtin-usermeta"><i class="far fa-fw fa-user" aria-hidden="true"></i><a href="/profil/<?php echo $linkProfile ?>"><?php $listing->the_author();?></a></li>
<!-- .autor -->
<!-- lokace -->
<li><i class="fa fa-fw fa-map-marker-alt" aria-hidden="true"></i><?php $listing->the_locations( true, false ); ?></li>
<!-- .lokace -->
<!-- kategorie -->
<li><i class="fa fa-list-ul" aria-hidden="true"></i> <a href="<?php echo esc_url( Link::get_category_page_link( $category ) ); ?>"><?php echo esc_html( $category->name ); ?></a> </li>
 <!-- .kategorie -->    
                
<!-- datum + zobrazení --> 
<div class="row">
    <div class="col-md-6 col-6">
<li><i class="far fa-fw fa-clock" aria-hidden="true"></i><?php $listing->the_time();?></li>
    </div>
    <div class="col-md-6 col-6">
<li><i class="fa fa-fw fa-eye" aria-hidden="true"></i><?php echo sprintf( esc_html__( '%1$s Views', 'classima' ) , number_format_i18n( $listing->get_view_counts() ) ); ?></li>
</div>
<!-- .datum + zobrazení -->
				</ul>



				<?php do_action( 'classima_listing_list_view_after_content', $listing );?>

			</div>
			<div class="rtin-right">
                <div class="typ <?php echo  "typ_".sanitize_title($type['label']); ?>">
				<?php if ( $display['type'] && $type ): ?>
				<?php echo esc_html( $type['label'] ); ?>
				<?php endif; ?>
                    </div>



                    <div class="rtin-quick-view">
                        <a class="rtcl-quick-view" href="#" data-listing_id="<?php echo absint($listing->get_id()) ?>">
                           <span><?php esc_html_e("Quick View", "classima"); ?></span>
                        </a>
                    </div>                
                    <div class="rtin-quick-view">
                        <a href="#">
                           <span>Inzerent</span>
                        </a>
                    </div>
                <?php if (Fns::is_enable_quick_view()) { ?>
                    <div class="rtin-quick-view">
                        <a href="<?php the_permalink(); ?>">
                            <span>Detail inzerátu</span>
                        </a>
                    </div>
                <?php } ?>

                <?php if (Fns::is_enable_compare()) {
                    $compare_ids = !empty($_SESSION['rtcl_compare_ids']) ? $_SESSION['rtcl_compare_ids'] : [];
                    $selected_class = '';
                    if (is_array($compare_ids) && in_array($listing->get_id(), $compare_ids)) {
                        $selected_class = ' selected';
                    }
                    ?>
                    <div class="rtin-compare">
                        <a class="rtcl-compare <?php echo esc_attr($selected_class); ?>" href="#" data-listing_id="<?php echo absint($listing->get_id()) ?>">
                            <i class="fa fa-retweet"></i><span><?php esc_html_e("Compare", "classima") ?></span>
                        </a>
                    </div>
                <?php } ?>
			</div>			
		</div>

	</div>
	<?php if ( $map ) $listing->the_map_lat_long();?>
</div>