<?php
/**
 * související
 */

namespace radiustheme\Classima;

use Rtcl\Helpers\Link;
use Rtcl\Helpers\Functions;
use RtclPro\Controllers\Hooks\TemplateHooks;

?>
<div class="listing-grid-each listing-grid-each-6<?php echo esc_attr( $class ); ?> <?php echo  "typb_".sanitize_title($type['label']); ?>">
    <div class="rtin-item">
        <div class="rtin-thumb">
            <a class="rtin-thumb-inner rtcl-media" href="<?php the_permalink(); ?>"><?php $listing->the_thumbnail(); ?></a>
                <div class="rtin-type typ <?php echo  "typ_".sanitize_title($type['label']); ?>"><span><?php echo sprintf(__(" %s", 'classima'), $type['label']); ?></span></div>
   
        </div>
<!-- obsahová část --> <div class="rtin-content">



<!-- štítky -->

<!-- .štítky -->
<div class="rtin-contentv">
<!-- nadpis -->
			<h3 class="rtin-title listing-title mujtithp" title="<?php the_title(); ?>"><a href="<?php the_permalink(); ?>"><?php echo wp_trim_words( get_the_title(), 8 ); ?></a></h3> 
<!-- .nadpis --></div>            


<!-- obsah kategorie,lokalita -->            
<div class="grid-obs-hp"> 
    <div class="rtin-contentv">          

            <ul class="rtin-meta grigseznam-hp">
 <!-- kategorie -->           
                <?php if ( $display['cat'] ): ?>
                <li><i class="fa fa-list-ul" aria-hidden="true"></i> <a href="<?php echo esc_url( Link::get_category_page_link( $category ) ); ?>"><?php echo esc_html( $category->name ); ?></a></li>
                <?php endif; ?>
<!-- .kategorie -->             
                           
<!-- lokalita -->
                    <li><i class="fa fa-fw fa-map-marker-alt" aria-hidden="true"></i><?php $listing->the_locations( true, false ); ?></li>
      
<!-- .lokalita -->  

<!-- zobrazení --> 
<li><i class="fa fa-fw fa-eye" aria-hidden="true"></i><?php echo sprintf( esc_html__( '%1$sx zobrazeno', 'classima' ) , number_format_i18n( $listing->get_view_counts() ) ); ?></li>
<!-- .zobrazení -->

            </ul>
            </div>
    </div>

<!-- .obsah kategorie,lokalita -->            
            
<!-- .obsahová část --> </div>
    </div>
</div>