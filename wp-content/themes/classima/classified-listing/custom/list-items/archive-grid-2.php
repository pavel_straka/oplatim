<?php
/**
 * NEPOUŽITO
 * 
 * @author  RadiusTheme
 * @since   1.0
 * @version 1.7
 */

namespace radiustheme\Classima;

use RtclPro\Controllers\Hooks\TemplateHooks;
use Rtcl\Helpers\Functions;
use Rtcl\Helpers\Link;
?>
<div class="listing-grid-each listing-grid-each-2<?php echo esc_attr( $class ); ?>">
	<div class="rtin-item">
        <div class="rtin-thumb">
            <a class="rtin-thumb-inner rtcl-media" href="<?php the_permalink(); ?>"><?php $listing->the_thumbnail(); ?></a>
            <?php TemplateHooks::sold_out_banner(); ?>
        </div>
		<div class="rtin-content">

			<?php if ( $display['price'] ): ?>
				<div class="rtin-price">
                    <?php
                    if (method_exists( $listing, 'get_price_html')) {
                        Functions::print_html($listing->get_price_html());
                    }
                    ?>
                </div>
			<?php endif; ?>

			<?php if ( $display['cat'] ): ?>
				<a class="rtin-cat" href="<?php echo esc_url( Link::get_category_page_link( $category ) ); ?>"><?php echo esc_html( $category->name ); ?></a>
			<?php endif; ?>

			<h3 class="rtin-title listing-title mujtit" title="<?php the_title(); ?>"><a href="<?php the_permalink(); ?>"><?php echo wp_trim_words( get_the_title(), 7 ); ?></a>G222</h3>
         
			<?php
			if ( $display['label'] ) {
				$listing->the_badges();
			}
			?>  



			<ul class="rtin-meta">
				<?php if ( $display['type'] && $type ): ?>
					<li><i class="fa fa-fw <?php echo esc_attr( $type['icon'] );?>" aria-hidden="true"></i><?php echo esc_html( $type['label'] ); ?></li>
				<?php endif; ?>
				<?php if ( $display['date'] ): ?>
					<li><i class="far fa-fw fa-clock" aria-hidden="true"></i><?php $listing->the_time();?></li>
				<?php endif; ?>
				<?php if ( $display['user'] ): ?>
					<li class="rtin-usermeta"><i class="far fa-fw fa-user" aria-hidden="true"></i><?php $listing->the_author();?></li>
				<?php endif; ?>
				<?php if ( $display['location'] && $listing->has_location() ): ?>
					<li><i class="fa fa-fw fa-map-marker" aria-hidden="true"></i><?php $listing->the_locations( true, false ); ?></li>
				<?php endif; ?>
                      <?php if ( $display['cat'] ): ?>
					<li><i class="fa fa-list-ul" aria-hidden="true"></i> <a href="<?php echo esc_url( Link::get_category_page_link( $category ) ); ?>"><?php echo esc_html( $category->name ); ?></a> </li>
				       <?php endif; ?>
			</ul>
		</div>
    
	</div>
	<?php if ( $map ) $listing->the_map_lat_long();?>
</div>