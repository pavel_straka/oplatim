<?php
/**
 * @author        RadiusTheme
 * @package       classified-listing/views/public
 * @version       1.0.0
 */

use Rtcl\Models\RtclCFGField;
use Rtcl\Models\Listing;

$listing = new Listing( get_the_id() );
$items = array();
$urls  = array(); 

if ( $listing->can_show_category() ) {
	$category = $listing->get_categories();
	$category = current( $category );
	if ( $category ) {
		$items[] = array(
			'label' => esc_html__( 'Category', 'classima' ),
			'value' => $category->name,
		);		
	}
}

foreach ( $fields as $field ) {
	$field = new RtclCFGField( $field->ID );
	$value = $field->getFormattedCustomFieldValue( $listing_id );
	if ( ! empty( $value ) ) {
		if ( $field->getType() === 'url' ) {
			$nofollow = ! empty( $field->getNofollow() ) ? ' rel="nofollow"' : '';
			$urls[] = sprintf( ' <a href="%1$s" target="%2$s"%3$s>%4$s</a>', $value, $field->getTarget(), $nofollow, $field->getLabel() );
		}
		else {
			$items[] = array(
				'label' => $field->getLabel(),
				'value' => $value,
			);
		}
	}
}

if ( !$items && !$urls ) {
	return;
}
?>



<hr>
<!-- kategorie + vlastní pole -->
<div class="row kat-meta">
     <div class="col-md-6 col-12">

		<span>
<!-- kategorie --> <i class="fas fa-tags ikona"></i><span class="rtin-title"><a href="/inzerat-kategorie/<?php echo sanitize_title($items[0]['value']) ?>"><?php echo wp_kses_post( $items[0]['value'] );?></a></span>
        </span>

    </div>
		<?php
		unset($items[0]);
        if(is_array($items) && count($items) > 0){
		foreach ( $items as $item ){ ?>
        <div class="col-md-6 col-12" style="text-align: right;"> 
         <span>
<!-- vlastní pole -->		<i class="fas fa-wallet ikona"></i><span class="rtin-label"><?php echo esc_html( $item['label'] );?>: </span>
<!-- cena -->				<span class="rtin-title" id="cenainz"><?php echo wp_kses_post( $item['value'] );?></span>
		</span>       
        
    </div>
<?php }} ?></div>
<!-- .kategorie + vlastní pole -->