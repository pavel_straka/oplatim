<?php
/**
 * @author  RadiusTheme
 * @since   1.0
 * @version 1.3
 */

namespace radiustheme\Classima;

use Rtcl\Helpers\Functions;
use Rtcl\Helpers\Link;
use RtclPro\Helpers\Fns;

global $store;
$store_oh_type = get_post_meta($store->get_id(), 'oh_type', true);
$store_oh_hours = get_post_meta($store->get_id(), 'oh_hours', true);
$store_oh_hours = is_array($store_oh_hours) ? $store_oh_hours : ($store_oh_hours ? (array)$store_oh_hours : array());

$today = strtolower(date('l'));

$days = array(
	'sunday'    => esc_html__( 'Sunday', 'classima' ),
	'monday'    => esc_html__( 'Monday', 'classima' ),
	'tuesday'   => esc_html__( 'Tuesday', 'classima' ),
	'wednesday' => esc_html__( 'Wednesday', 'classima' ),
	'thursday'  => esc_html__( 'Thursday', 'classima' ),
	'friday'    => esc_html__( 'Friday', 'classima' ),
	'saturday'  => esc_html__( 'Saturday', 'classima' ),
);
?>



<div class="classima-store-info widget">
	<div>
        <?php if (Fns::registered_user_only('store_contact') && !is_user_logged_in()) { ?>
            <p class="login-message">
                <?php echo wp_kses(sprintf(__("Please <a href='%s'>login</a> to view the store contact.", "classima"), esc_url(Link::get_my_account_page_link())), ['a' => ['href' => []]]); ?>
            </p>
        <?php } else { ?>

<!-- sidebar info -->      
                <div class="rtin-location rtin-box-item clearfix">
                    <div class="rtin-box-item-text adresaprofil">
<!-- adresa -->   
                    <i class="fas fa-map-marker-alt ikona" aria-hidden="true"></i>  <?php
                        $shop = $store->get_id();
                        $shopParam = get_post($shop);
                        $location = get_user_meta($shopParam->post_author,'_rtcl_address',true); 
                        $location2 = get_user_meta($shopParam->post_author,'_rtcl_location',true);
                        $region = get_term_by('id', $location2[0], 'rtcl_location');
                        $district = get_term_by('id', $location2[1], 'rtcl_location');  
                        if($location){
                          if($region && $district){
                                echo $region->name.', '.$district->name.'<br><i class="fas fa-street-view ikona" aria-hidden="true"></i>';
                           }
                            echo '<span style="color: #858585;font-weight: 400;">' .wp_kses_post( $location ). '</span>';  
                        }?></span><br>
<!-- .adresa -->
<!-- stav uživatele -->
    <?php
    global $post;
    if( is_user_online( $post->post_author ) ) : ?>
           <i class="fas fa-user-check ikona"></i>Uživatel je <span class="onl">online</span>
    <?php else : ?>
           <i class="fas fa-user-times ikonaoff"></i>Uživatel je <span class="offl">offline</span>
    <?php endif; ?> <br> 
<!-- .stav uživatele -->           
<!-- clenství -->      
                    <i class="fa fa-user ikona" aria-hidden="true"></i>   <?php echo esc_html( $member_since );?>                    
<!-- .clenství -->  
                    </div>    
                </div>
<!-- .sidebar info --> 


            <?php if ( $store_phone = $store->get_phone() ): ?>
            
                <div class="rtin-phone"><?php Listing_Functions::the_phone( $store_phone );?></div>   
            <?php endif; ?>

            <?php if ( $store_email = $store->get_email() ) : ?>
                <div class="media rtin-email">
                    <a data-toggle="modal" data-target="#classima-mail-to-seller" href="#"><i class="fas fa-envelope" aria-hidden="true"></i><?php esc_html_e( 'Message Store Owner', 'classima' );?></a>

                    <div class="modal fade" id="classima-mail-to-seller" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" data-hide="0"><?php Functions::get_template( 'store/contact-form', null, '', rtclStore()->get_plugin_template_path() ); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php } ?>

	</div>

    
<div class="classima-store-details widget">
	<div>
<div class="mezerai"> </div>   
<!-- popis -->

<p><b>O mě</b></p> 
<div class="linka"> </div>
<div class="profpopis">
<p><?php echo $store->get_the_description(); ?></p>
</div>
<!-- .popis -->

<!-- social ikony -->
<?php if ( $store->get_social_media() ): ?>
	        <div class="classima-store-socials"><?php echo wp_kses_post( $store->get_social_media_html() ); ?></div>
<?php endif; ?>
<!-- social ikony -->

	</div>
</div>    

</div>