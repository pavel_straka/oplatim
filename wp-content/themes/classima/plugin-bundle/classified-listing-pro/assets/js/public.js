(function ($) {
  $(document).on('click', '.mark-as-sold', function (e) {
    e.preventDefault();

    var _target = this,
        _self = $(_target),
        data = {
      action: 'rtcl_mark_as_sold_unsold',
      post_id: parseInt(_self.attr("data-id"), 10),
      __rtcl_wpnonce: rtcl.__rtcl_wpnonce
    };

    if (data.post_id && !_self.hasClass('working')) {
      $.ajax({
        url: rtcl.ajaxurl,
        data: data,
        type: "POST",
        beforeSend: function beforeSend() {
          _self.addClass('working');
        },
        success: function success(res) {
          _self.removeClass('working');

          res.target = _target;

          if (res.success) {
            _self.attr('data-title', res.data.text).attr('data-tooltip', res.data.text).html(res.data.text);

            if (res.data.type === 'sold') {
              _self.addClass('sold');
            } else {
              _self.removeClass('sold');
            }
          }

          $(document).trigger('rtcl.mark_as_sold', res);
        },
        error: function error(e) {
          $(document).trigger('rtcl.mark_as_sold.error', {
            listing_id: data.post_id,
            target: _target
          });

          _self.removeClass('working');
        }
      });
    }
  });
})(jQuery);
