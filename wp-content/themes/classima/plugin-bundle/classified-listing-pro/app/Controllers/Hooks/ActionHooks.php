<?php

namespace RtclPro\Controllers\Hooks;


use Rtcl\Models\Payment;
use Rtcl\Models\Roles;
use RtclPro\Helpers\Fns;
use Rtcl\Helpers\Functions;
use RtclPro\Helpers\Options;
use RtclPro\Gateways\WooPayment\WooPayment;

class ActionHooks
{
    public static function init() {
        add_action('init', [__CLASS__, "wc_payment_support"]);
        add_action('rtcl_admin_settings_before_saved_account_settings', [__CLASS__, 'apply_user_role_at_account_settings'], 10, 2);
        add_action('rtcl_listing_query', [__CLASS__, 'add_geo_query'], 10, 2);
        add_action('rtcl_listing_overwrite_change', [__CLASS__, 'update_promotions_at_save_post'], 10, 2);
        add_action('rtcl_listing_update_metas_at_admin', [__CLASS__, 'update_listings_meta_at_save_post'], 10, 2);


        add_action('rtcl_save_pricing_meta_data', [__CLASS__, 'save_pricing_meta_data'], 10, 2);

        add_action('rtcl_cron_move_listing_publish_to_expired', [__CLASS__, 'remove_data_move_listing_publish_to_expired']);
        add_action('rtcl_cron_hourly_scheduled_events', [__CLASS__, 'cron_hourly_scheduled_actions']);

        add_action('rtcl_rest_checkout_process_success', [__CLASS__, 'rest_checkout_process_mail']);

        add_action('clear_auth_cookie', [__CLASS__, 'set_user_status_offline']);
    }


    public static function set_user_status_offline() {
        update_user_meta(get_current_user_id(), 'online_status', 0);
        delete_user_meta(get_current_user_id(), '_rtcl_conversation_status');
    }


    /**
     * @param Payment $payment
     */
    static function rest_checkout_process_mail($payment) {
        if ($payment && $payment->exists()) {
            if (Functions::get_option_item('rtcl_email_settings', 'notify_admin', 'order_created', 'multi_checkbox')) {
                rtcl()->mailer()->emails['Order_Created_Email_To_Admin']->trigger($payment->get_id(), $payment);
            }

            if (Functions::get_option_item('rtcl_email_settings', 'notify_users', 'order_created', 'multi_checkbox')) {
                rtcl()->mailer()->emails['Order_Created_Email_To_Customer']->trigger($payment->get_id(), $payment);
            }
        }
    }

    /**
     * @param integer $post_id
     * @param array   $request
     */
    public static function save_pricing_meta_data($post_id, $request) {
        if (isset($request['_top'])) {
            update_post_meta($post_id, '_top', 1);
        } else {
            delete_post_meta($post_id, '_top');
        }
        if (isset($request['_bump_up'])) {
            update_post_meta($post_id, '_bump_up', 1);
        } else {
            delete_post_meta($post_id, '_bump_up');
        }
    }

    public static function cron_hourly_scheduled_actions() {
        self::remove_expired_bump_up();
        self::remove_expired_top_listing();
        self::do_hourly_bump_up();
    }

    public static function remove_data_move_listing_publish_to_expired($post_id) {
        delete_post_meta($post_id, '_top');
        delete_post_meta($post_id, '_top_expiry_date');
        delete_post_meta($post_id, '_bump_up');
        delete_post_meta($post_id, '_bump_up_expiry_date');
    }

    private static function do_hourly_bump_up() {
        // Define the query
        $args = apply_filters('rtcl_cron_do_hourly_bump_up_query_args', [
            'post_type'      => rtcl()->post_type,
            'posts_per_page' => 10,
            'post_status'    => 'publish',
            'fields'         => 'ids',
            'date_query'     => [
                'before' => current_time('Y-m-d')
            ],
            'meta_query'     => [
                'relation' => 'AND',
                [
                    'key'     => '_bump_up_expiry_date',
                    'value'   => current_time('mysql'),
                    'compare' => '>',
                    'type'    => 'DATETIME'
                ],
                [
                    'key'     => '_bump_up',
                    'compare' => '=',
                    'value'   => 1,
                ]
            ]
        ]);

        $rtcl_query = new \WP_Query($args);
        if (!empty($rtcl_query->posts)) {
            foreach ($rtcl_query->posts as $post_id) {
                wp_update_post(
                    array(
                        'ID'            => $post_id,
                        'post_date'     => current_time('mysql'),
                        'post_date_gmt' => get_gmt_from_date(current_time('mysql'))
                    )
                );
                do_action("rtcl_cron_do_hourly_bump_up_listing", $post_id);
            }
        }
    }

    private static function remove_expired_bump_up() {

        // Define the query
        $args = array(
            'post_type'      => rtcl()->post_type,
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'fields'         => 'ids',
            'meta_query'     => array(
                'relation' => 'AND',
                array(
                    'key'     => '_bump_up_expiry_date',
                    'value'   => current_time('mysql'),
                    'compare' => '<',
                    'type'    => 'DATETIME'
                ),
                array(
                    'key'     => '_bump_up',
                    'compare' => '=',
                    'value'   => 1,
                )
            )
        );


        $rtcl_query = new \WP_Query(apply_filters('rtcl_cron_remove_expired_bump_up_query_args', $args));

        if (!empty($rtcl_query->posts)) {

            foreach ($rtcl_query->posts as $post_id) {
                delete_post_meta($post_id, '_bump_up');
                delete_post_meta($post_id, '_bump_up_expiry_date');
                do_action("rtcl_cron_remove_expired_bump_up_listing", $post_id); // TODO : make task
            }
        }


    }

    private static function remove_expired_top_listing() {
        // Define the query
        $args = array(
            'post_type'      => rtcl()->post_type,
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'fields'         => 'ids',
            'meta_query'     => array(
                'relation' => 'AND',
                array(
                    'key'     => '_top_expiry_date',
                    'value'   => current_time('mysql'),
                    'compare' => '<',
                    'type'    => 'DATETIME'
                ),
                array(
                    'key'     => '_top',
                    'compare' => '=',
                    'value'   => 1,
                )
            )
        );


        $rtcl_query = new \WP_Query(apply_filters('rtcl_cron_remove_expired_top_listing_query_args', $args));

        if (!empty($rtcl_query->posts)) {

            foreach ($rtcl_query->posts as $post_id) {
                delete_post_meta($post_id, '_top');
                delete_post_meta($post_id, '_top_expiry_date');
                do_action("rtcl_cron_remove_expired_top_listing", $post_id);
            }
        }
    }

    public static function update_listings_meta_at_save_post($post_id, $request) {
        if (isset($request['rtcl_geo_address']) && "google" === Fns::location_type()) {
            update_post_meta($post_id, '_rtcl_geo_address', Functions::sanitize($request['rtcl_geo_address']));
        }
    }

    /**
     * @param integer $post_id
     * @param array   $request
     */
    public static function update_promotions_at_save_post($post_id, $request) {

        // Top
        if (isset($request['_top'])) {
            update_post_meta($post_id, '_top', 1);
        } else {
            delete_post_meta($post_id, '_top');
            delete_post_meta($post_id, '_top_expiry_date');
        }

        // Bump up
        if (isset($request['_bump_up'])) {
            update_post_meta($post_id, '_bump_up', 1);
        } else {
            delete_post_meta($post_id, '_bump_up');
            delete_post_meta($post_id, '_bump_up_expiry_date');
        }
    }

    public static function add_geo_query($query, $qObj) {
        $distance = !empty($_GET['distance']) ? absint($_GET['distance']) : 0;
        $rtcl_geo_query = $query->get('rtcl_geo_query', []);

        if ($distance) {
            $current_user_id = get_current_user_id();
            $lat = !empty($_GET['center_lat']) ? trim($_GET['center_lat']) : get_user_meta($current_user_id, '_rtcl_latitude', true);
            $lan = !empty($_GET['center_lng']) ? trim($_GET['center_lng']) : get_user_meta($current_user_id, '_rtcl_longitude', true);

            if ($lat && $lan) {
                $rs_data = Options::radius_search_options();
                $rtcl_geo_query = [
                    'lat_field' => 'latitude',
                    'lng_field' => 'longitude',
                    'latitude'  => $lat,
                    'longitude' => $lan,
                    'distance'  => $distance,
                    'units'     => $rs_data["units"]
                ];
            }
        }
        $geo_query = array_filter(apply_filters('rtcl_listing_query_geo_query', $rtcl_geo_query, $qObj));
        $query->set('rtcl_geo_query', $geo_query);
    }

    /**
     * Appends tax queries to an array.
     *
     * @param array $geo_query GEO query.
     *
     * @return array
     */
    public function get_geo_query(array $geo_query = []) {

    }

    public static function wc_payment_support() {
        if (Fns::is_wc_payment_enabled()) {
            new WooPayment();
        }
    }


    public static function apply_user_role_at_account_settings($new_options, $old_options) {

        if (!empty($new_options['allowed_core_permission_roles']) || !empty($old_options['allowed_core_permission_roles'])) {

            $new_roles = isset($new_options['allowed_core_permission_roles']) && is_array($new_options['allowed_core_permission_roles']) ? $new_options['allowed_core_permission_roles'] : [];
            $old_roles = isset($old_options['allowed_core_permission_roles']) && is_array($old_options['allowed_core_permission_roles']) ? $old_options['allowed_core_permission_roles'] : [];
            $add_roles = array_diff($new_roles, $old_roles);
            $remove_roles = array_diff($old_roles, $new_roles);
            if (!empty($add_roles)) {
                Roles::add_core_caps($add_roles);
            }
            if (!empty($remove_roles)) {
                Roles::remove_code_caps($remove_roles);
            }
        }

    }
}
