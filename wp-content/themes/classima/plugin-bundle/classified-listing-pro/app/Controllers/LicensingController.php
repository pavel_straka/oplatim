<?php

namespace RtclPro\Controllers;

use Rtcl\Helpers\Functions;
use RtclPro\Helpers\Fns;
use RtclPro\Models\RtclLicense;

class LicensingController
{

    // Licensing variable
    static private $store_url = 'https://www.radiustheme.com';
    static private $product_id = 81839;
    static private $author = "RadiusTheme";

    static function init() {
        add_action('admin_init', [__CLASS__, 'license']);
        add_action('rtcl_admin_settings_saved', [__CLASS__, 'update_licencing_status']);
        add_action('wp_ajax_rtcl_manage_licensing', [__CLASS__, 'rtcl_manage_licensing']);
    }

    static function license() {
        if (Fns::check_license()) {
            $settings = Functions::get_option('rtcl_tools_settings');
            $license_key = !empty($settings['license_key']) ? trim($settings['license_key']) : null;
            $status = !empty($settings['license_status']) && $settings['license_status'] === 'valid';
            new RtclLicense(static::$store_url, RTCL_PRO_PLUGIN_FILE, [
                'version' => RTCL_PRO_VERSION,        // current version number
                'license' => $license_key,    // license key (used get_option above to retrieve from DB)
                'item_id' => self::$product_id,    // id of this plugin
                'author'  => self::$author,    // author of this plugin
                'url'     => home_url(),
                'beta'    => false,
                'status'  => $status
            ]);
        }
    }

    static function rtcl_manage_licensing() {
        $error = true;
        $type = $value = $data = $message = null;
        $settings = Functions::get_option('rtcl_tools_settings');
        $license_key = !empty($settings['license_key']) ? trim($settings['license_key']) : null;
        if (!empty($_REQUEST['type']) && $_REQUEST['type'] == "license_activate") {
            $api_params = array(
                'edd_action' => 'activate_license',
                'license'    => $license_key,
                'item_id'    => self::$product_id,
                'url'        => home_url()
            );
            $response = wp_remote_post(self::$store_url,
                array('timeout' => 15, 'sslverify' => false, 'body' => $api_params));
            if (is_wp_error($response) || 200 !== wp_remote_retrieve_response_code($response)) {
                if (is_wp_error($response) && !empty($response->get_error_message())) {
                    $message = $response->get_error_message();
                } elseif (isset($response['response']) && is_array($response['response']) && isset($response['response']['message'])) {
                    $message = $response['response']['message'];
                } else {
                    $message = esc_html__('An error occurred, please try again.', 'classified-listing-pro');
                }
            } else {
                $license_data = json_decode(wp_remote_retrieve_body($response));
                if (false === $license_data->success) {
                    switch ($license_data->error) {
                        case 'expired' :
                            $message = sprintf(
                                esc_html__('Your license key expired on %s.', 'classified-listing-pro'),
                                date_i18n(get_option('date_format'),
                                    strtotime($license_data->expires, current_time('timestamp')))
                            );
                            break;
                        case 'revoked' :
                            $message = esc_html__('Your license key has been disabled.', 'classified-listing-pro');
                            break;
                        case 'missing' :
                            $message = esc_html__('Invalid license.', 'classified-listing-pro');
                            break;
                        case 'invalid' :
                        case 'site_inactive' :
                            $message = esc_html__('Your license is not active for this URL.', 'classified-listing-pro');
                            break;
                        case 'item_name_mismatch' :
                            $message = esc_html__('This appears to be an invalid license key for Classified Listing Pro.', 'classified-listing-pro');
                            break;
                        case 'no_activations_left':
                            $message = esc_html__('Your license key has reached its activation limit.', 'classified-listing-pro');
                            break;
                        default :
                            $message = esc_html__('An error occurred, please try again.', 'classified-listing-pro');
                            break;
                    }
                }
                // Check if anything passed on a message constituting a failure
                if (empty($message)) {
                    $settings['license_status'] = $license_data->license;
                    update_option('rtcl_tools_settings', $settings);
                    $error = false;
                    $type = 'license_deactivate';
                    $value = esc_html__('Deactivate License', 'classified-listing-pro');
                    $message = esc_html__("License successfully activated", 'classified-listing-pro');
                }
            }
        }
        if (!empty($_REQUEST['type']) && $_REQUEST['type'] == "license_deactivate") {
            $api_params = array(
                'edd_action' => 'deactivate_license',
                'license'    => $license_key,
                'item_id'    => self::$product_id,
                'url'        => home_url()
            );
            $response = wp_remote_post(self::$store_url,
                array('timeout' => 15, 'sslverify' => false, 'body' => $api_params));

            // Make sure there are no errors
            if (is_wp_error($response) || 200 !== wp_remote_retrieve_response_code($response)) {
                if (is_wp_error($response) && !empty($response->get_error_message())) {
                    $message = $response->get_error_message();
                } elseif (isset($response['response']) && is_array($response['response']) && isset($response['response']['message'])) {
                    $message = $response['response']['message'];
                } else {
                    $message = esc_html__('An error occurred, please try again.', 'classified-listing-pro');
                }
            } else {
                unset($settings['license_status']);
                update_option('rtcl_tools_settings', $settings);
                $error = false;
                $type = 'license_activate';
                $value = esc_html__('Activate License', 'classified-listing-pro');
                $message = esc_html__("License successfully deactivated", 'classified-listing-pro');
            }
        }
        $response = array(
            'error' => $error,
            'msg'   => $message,
            'type'  => $type,
            'value' => $value,
            'data'  => $data
        );
        wp_send_json($response);
    }

    static function update_licencing_status($action) {
        if ("tools_settings" == $action) {
            $settings = Functions::get_option('rtcl_tools_settings');
            $license_key = !empty($settings['license_key']) ? trim($settings['license_key']) : null;
            $status = (!empty($settings['license_status']) && $settings['license_status'] === 'valid') ? true : false;
            if ($license_key && !$status) {
                $api_params = array(
                    'edd_action' => 'activate_license',
                    'license'    => $license_key,
                    'item_id'    => self::$product_id,
                    'url'        => home_url()
                );
                $response = wp_remote_post(self::$store_url,
                    array('timeout' => 15, 'sslverify' => false, 'body' => $api_params));
                if (is_wp_error($response) || 200 !== wp_remote_retrieve_response_code($response)) {
                    if (is_wp_error($response) && !empty($response->get_error_message())) {
                        $message = $response->get_error_message();
                    } elseif (isset($response['response']) && is_array($response['response']) && isset($response['response']['message'])) {
                        $message = $response['response']['message'];
                    } else {
                        $message = esc_html__('An error occurred, please try again.', 'classified-listing-pro');
                    }
                    Functions::add_notice($message ? $message : esc_html__('Error to activation license', 'classified-listing-pro'), 'error');
                } else {
                    $license_data = json_decode(wp_remote_retrieve_body($response));
                    if (false === $license_data->success) {
                        switch ($license_data->error) {
                            case 'expired' :
                                $message = sprintf(
                                    esc_html__('Your license key expired on %s.', 'classified-listing-pro'),
                                    date_i18n(get_option('date_format'),
                                        strtotime($license_data->expires, current_time('timestamp')))
                                );
                                break;
                            case 'revoked' :
                                $message = esc_html__('Your license key has been disabled.', 'classified-listing-pro');
                                break;
                            case 'missing' :
                                $message = esc_html__('Invalid license.', 'classified-listing-pro');
                                break;
                            case 'invalid' :
                            case 'site_inactive' :
                                $message = esc_html__('Your license is not active for this URL.', 'classified-listing-pro');
                                break;
                            case 'item_name_mismatch' :
                                $message = esc_html__('This appears to be an invalid license key for Classified Listing Pro.', 'classified-listing-pro');
                                break;
                            case 'no_activations_left':
                                $message = esc_html__('Your license key has reached its activation limit.', 'classified-listing-pro');
                                break;
                            default :
                                $message = esc_html__('An error occurred, please try again.', 'classified-listing-pro');
                                break;
                        }
                    }
                    // Check if anything passed on a message constituting a failure
                    if (empty($message) && $license_data->license === 'valid') {
                        $settings['license_status'] = $license_data->license;
                        update_option('rtcl_tools_settings', $settings);
                        Functions::add_notice(esc_html__('Successfully activated', 'classified-listing-pro'), 'success');
                    } else {
                        Functions::add_notice($message ? $message : esc_html__('Error to activation license', 'classified-listing-pro'), 'error');
                    }
                }

            } else if (!$license_key && !$status) {
                unset($settings['license_status']);
                update_option('rtcl_tools_settings', $settings);
            }
        }
    }
}