<?php
/**
 * @author  RadiusTheme
 * @since   1.0
 * @version 1.7
 */

namespace radiustheme\Classima;

use Rtcl\Helpers\Link;
use Rtcl\Helpers\Functions;
use RtclPro\Controllers\Hooks\TemplateHooks;
use RtclPro\Helpers\Fns;

$phone = get_post_meta( $listing_post->ID, 'phone', true );
?>
<div class="listing-list-each listing-list-each-6<?php echo esc_attr( $class ); ?>">
	<div class="rtin-item">
        <div class="rtin-thumb catobr">
            <a class="rtin-thumb-inner rtcl-media" href="<?php the_permalink(); ?>"><div class="catobr2"><?php $listing->the_thumbnail(); ?></div></a>
            <?php TemplateHooks::sold_out_banner(); ?>
        </div>
    
        
        
		<div class="rtin-content-area">
			<div class="rtin-content">                                                       

				
				<h3 class="rtin-title listing-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3><br>
 




			1	<?php
    static function listing_bump_up_badge($listing) {

        $display_option = is_singular(rtcl()->post_type) ? 'display_options_detail' : 'display_options';
        $can_show = apply_filters('rtcl_listing_can_show_top_badge', true, $listing);
        $can_show_settings = Functions::get_option_item('rtcl_moderation_settings', $display_option, 'bump_up', 'multi_checkbox');
        if (!$can_show || !$can_show_settings || !get_post_meta($listing->get_id(), '_bump_up', true)) {
            return;
        }

        $label = Functions::get_option_item('rtcl_moderation_settings', 'listing_bump_up_label');
        $label = $label ?: esc_html__("Bump Up", "classified-listing-pro");
        echo '<span class="badge rtcl-badge-_bump_up">' . esc_html($label) . '</span>';
    }
				?> 2



				<ul class="rtin-meta">
					<?php if ( $display['date'] ): ?>
						<li><i class="far fa-fw fa-clock" aria-hidden="true"></i><?php $listing->the_time();?></li>
					<?php endif; ?>
					<?php if ( $display['user'] ): ?>
						<li class="rtin-usermeta"><i class="far fa-fw fa-user" aria-hidden="true"></i><?php $listing->the_author();?></li>
					<?php endif; ?>
					<?php if ( $display['location'] && $listing->has_location() ): ?>
						<li><i class="fa fa-fw fa-map-marker" aria-hidden="true"></i><?php $listing->the_locations( true, false ); ?></li>
					<?php endif; ?>
					<?php if ( $display['views'] ): ?>
						<li><i class="fa fa-fw fa-eye" aria-hidden="true"></i><?php echo sprintf( esc_html__( 'Visit: %1$s', 'classima' ) , number_format_i18n( $listing->get_view_counts() ) ); ?></li>
					<?php endif; ?>
                      <?php if ( $display['cat'] ): ?>
					<li><i class="fa fa-list-ul" aria-hidden="true"></i> <a href="<?php echo esc_url( Link::get_category_page_link( $category ) ); ?>"><?php echo esc_html( $category->name ); ?></a> </li>
				       <?php endif; ?>
				</ul>

				<?php if ( $display['excerpt'] ): ?>
ÿ 		⿠ؤ  hp
					$excerpt = Helper::get_current_post_content( $listing_post );
					$excerpt = wp_trim_words( $excerpt, $display['excerpt_limit'] );
					?>
					<p class="rtin-excerpt"><?php echo esc_html( $excerpt ); ?></p>
				<?php endif; ?>

				<?php do_action( 'classima_listing_list_view_after_content', $listing );?>

			</div>
			<div class="rtin-right">
                <div class="typ xxx">
				<?php if ( $display['type'] && $type ): ?>
				<?php echo esc_html( $type['label'] ); ?>
				<?php endif; ?>
                    </div>



                    <div class="rtin-quick-view">
                        <a class="rtcl-quick-view" href="#" data-listing_id="<?php echo absint($listing->get_id()) ?>">
                            <i class="fas fa-search-plus"></i><span><?php esc_html_e("Quick View", "classima"); ?></span>
                        </a>
                    </div>                

                <?php if (Fns::is_enable_quick_view()) { ?>
                    <div class="rtin-quick-view">
                        <a href="<?php the_permalink(); ?>">
                            <i class="fas fa-info-circle"></i><span>Detail inzerátu</span>
                        </a>
                    </div>
                <?php } ?>

                <?php if (Fns::is_enable_compare()) {
                    $compare_ids = !empty($_SESSION['rtcl_compare_ids']) ? $_SESSION['rtcl_compare_ids'] : [];
                    $selected_class = '';
                    if (is_array($compare_ids) && in_array($listing->get_id(), $compare_ids)) {
                        $selected_class = ' selected';
                    }
                    ?>
                    <div class="rtin-compare">
                        <a class="rtcl-compare <?php echo esc_attr($selected_class); ?>" href="#" data-listing_id="<?php echo absint($listing->get_id()) ?>">
                            <i class="fa fa-retweet"></i><span><?php esc_html_e("Compare", "classima") ?></span>
                        </a>
                    </div>
                <?php } ?>
			</div>			
		</div>

	</div>
	<?php if ( $map ) $listing->the_map_lat_long();?>
</div>