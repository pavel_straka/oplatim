<div class="wrap">
	
	<h1 class="wp-heading-inline">Příklady</h1>
    <hr class="wp-header-end">
    	<div class="about-text">
    
<p><strong>Základní:</strong></p>      
<pre><p>&lt;?php the_field('JM&Eacute;NO POLE'); ?&gt;</p></pre>
 <hr>   
<p><strong>Základní pro celý web:</strong></p>      
<pre><p>&lt;?php the_field('textove_pole1', 'option'); ?&gt;</p></pre>
<hr>
<p><strong>Short kód:</strong></p>      
<pre><p>[acf field="xxx" post_id="option"]</p></pre>  
<hr>
<p><strong>Povilit/zakázat obsah:</strong></p>      
<pre>
<p>&lt;!-- vlastn&iacute; obsah povolen --&gt; <br />&lt;?php if ( get_field( 'pridat_soubor-ano', 'option' ) ): ?&gt;<br />&lt;p&gt; &lt;?php the_field('soubor_k_nabidkce', 'option' ); ?&gt; &lt;/p&gt;<br />&lt;?php else: ?&gt;<br />&lt;?php endif; ?&gt;<br />&lt;!-- vlastn&iacute; obsah povolen --&gt;</p>

</pre> 
<hr>
<p><strong>Opakovač + DIV:</strong></p>      
<pre>
<p>&lt;?php if(get_field('sada_5_fotografii', 'option')): ?&gt;<br />&lt;div class="radek skupina"&gt;<br />&lt;?php while(has_sub_field('sada_5_fotografii', 'option')): ?&gt;<br />&lt;div class="sloupec box-1-5"&gt;<br />
&lt;a href="&lt;?php the_sub_field('fotografie', 'option'); ?&gt;"&gt;<br />&lt;img src="&lt;?php the_sub_field('fotografie', 'option'); ?&gt;" width="100%" height="auto" /&gt;<br />&lt;/a&gt; <br />&lt;/div&gt;<br />&lt;?php endwhile; ?&gt;<br />&lt;/div&gt;</p>
<p>&lt;?php endif; ?&gt;</p>
</pre> 
<hr>
<p><strong>OPAKOVAČ + DOKUMENTY KE STAŽENÍ + NÁZEV SOUBORU:</strong></p>      
<pre>
<p>&lt;?php if(get_field('HLAVNI', 'option')): ?&gt;<br />&lt;ul&gt;<br />&lt;?php while(has_sub_field('HLAVNI', 'option')): ?&gt;</p>
<p>&lt;li&gt;&lt;a href="&lt;?php the_sub_field('XXX', 'option'); ?&gt;" download&gt;&lt;?php the_sub_field('XXX', 'option'); ?&gt;&lt;/a&gt;&lt;/li&gt;</p>
<p>&lt;?php endwhile; ?&gt;<br />&lt;/ul&gt;<br />&lt;?php endif; ?&gt;</p>
</pre> 



<hr>
<p><strong>OPAKOVAČ + NÁZEV + ODKAZ NA WEB:</strong></p>      
<pre>
<p>&lt;?php if(get_field('seznam_partneru', 'option')): ?&gt;<br />&lt;ul&gt;<br />&lt;?php while(has_sub_field('seznam_partneru', 'option')): ?&gt;<br />&lt;li&gt;&lt;?php the_sub_field('nazev_partnera', 'option'); ?&gt; | &lt;a href="&lt;?php the_sub_field('odkaz_na_partnera', 'option'); ?&gt;" target="_blank"&gt; &lt;?php the_sub_field('odkaz_na_partnera', 'option'); ?&gt;&lt;/a&gt;&lt;/li&gt;<br />&lt;?php endwhile; ?&gt;<br />&lt;/ul&gt;<br />&lt;?php endif; ?&gt;</p>
</pre> 

<hr>
<p><strong>DOKUMENTY KE STAŽENÍ + NÁZEV SOUBORU:</strong></p>      
<pre>
<p>&lt;p&gt;&lt;a href="&lt;?php the_field('soubor_ke_stazeni'); ?&gt;" download&gt;&lt;?php the_field('nazev_souboru'); ?&gt;&lt;/a&gt;&lt;/p&gt;</p>
</pre> 


<hr>
<p><strong>EMAIL + MAILTO:</strong></p>      
<pre>
<p>&lt;a href="mailto:&lt;?php the_field('email-sauna', 'option'); ?&gt;"&gt;&lt;?php the_field('email-sauna', 'option'); ?&gt;&lt;/a&gt;</p>

</pre> 

<hr>
<p><strong>OBRÁZEK BEZ ODKAZU:</strong></p>      
<pre>
<p>&lt;img src="&lt;?php the_field('fotka-stanoviste'); ?&gt;" alt="&lt;?php the_field('umisteni_stanoviste-kont'); ?&gt;" width="874px" height="auto" /&gt;</p>
</pre> 
  
<hr>
<p><strong>MINIATURA S PROKLIKEM NA ZVĚTŠENÍ ---- ( PLUGIN jQuery Colorbox):</strong></p>      
<pre>
<p>&lt;a href="&lt;?php the_field('obrazek'); ?&gt;" title="&lt;?php echo $title; ?&gt;"&gt;<br />&lt;img src="&lt;?php the_field('obrazek'); ?&gt;" alt="&lt;?php echo $alt; ?&gt;" width="100px" height="auto" /&gt;<br />&lt;/a&gt;</p>

</pre> 
<hr>
<p><strong>FOTOGALERIE:</strong></p>      
<pre>

<p>&lt;?php <br />$image_ids = get_field('fotogalerie', false, false);<br />$shortcode = '[gallery ids="' . implode(',', $image_ids) . '" columns="5"]';<br />echo do_shortcode( $shortcode );<br />?&gt;</p>


</pre>
<hr>
<p><strong>FOTOGALERIE - oprava:</strong></p>      
<pre>
<p>&lt;?php <br />$image_ids = get_field('gallery', false, false);<br />$image_ids = array();<br />$shortcode = '[gallery ids="' . implode(',', $image_ids) . '"]';<br />echo do_shortcode( $shortcode ); <br />?&gt;</p>
</pre>


<pre>
<hr>
<p><strong>FOTOGALERIE - DO ŠABLONY:</strong></p> 
<p>&lt;?php 
$image_ids = get_field('fotogalerie_vyrobku', false, false);
if (is_array($image_ids)) {
    $shortcode = '[ux_gallery ids="' . implode(',', $image_ids) . '" grid="null" style="normal" col_spacing="small" columns="3" image_size="original" text_align="center" image_hover="zoom" image_height="180px"]';
    echo do_shortcode( $shortcode );
}
?&gt; 
</p>
</pre>

    
    </div>

 <!-- kody pro šablonu -->
	<h1 class="wp-heading-inline">Kódy pro šablonu</h1>
    <hr class="wp-header-end">
    	<div class="about-text">
 
 <p><strong>SLOUPCE</strong></p>
<pre>      
 &lt;div class=&quot;large-12 col&quot;&gt;
		&lt;div class=&quot;row&quot;&gt;
&lt;div class=&quot;col medium-4 small-12 large-4&quot;&gt;1&lt;/div&gt;<br />
&lt;div class=&quot;col medium-4 small-12 large-4&quot;&gt;2&lt;/div&gt;<br />
&lt;div class=&quot;col medium-4 small-12 large-4&quot;&gt;3&lt;/div&gt;<br />
&lt;/div&gt;
 &lt;/div&gt;
</pre>

        
<p><strong>VYSKAKOVACÍ OKNO - PO KLIKNUTÍ NA TLAČÍTKO:</strong></p>
<pre>      
[button text="Lightbox button" link="#test"]
[lightbox id="test" width="600px" padding="20px"]
  Add lightbox content here...
[/lightbox]
</pre> 

 <p><strong>VYSKAKOVACÍ OKNO  - Auto open lightbox (vždy se otevře návštěvníkovi)</strong></p>
<pre>      
[lightbox auto_open="true" auto_timer="3000" auto_show="always" id="newsletter-signup-link" width="600px" padding="20px"]  
[ux_banner  bg="http://imageurl" height="400px" link="" animation="fadeInLeft" text_align="left" text_pos="left top" text_color="dark" text_width="40%"]
<h3>Signup for Newsletter</h3>
<div class="tx-div medium"></div>
OBSAH
[/ux_banner]
[/lightbox]
</pre> 

<p><strong>VYSKAKOVACÍ OKNO  - Auto open lightbox  (zobrazí se návštěvníkovi pouze jednou)</strong></p>
<pre>      
[lightbox auto_open="true" auto_timer="3000" auto_show="once" id="newsletter-signup-link" width="600px" padding="20px"]
 
[ux_banner  bg="http://imageurl" height="400px" link="" animation="fadeInLeft" text_align="left" text_pos="left top" text_color="dark" text_width="40%"]
<h3>Signup for Newsletter</h3>
<div ​class="tx-div medium"></div>
OBSAH
[/ux_banner]
</pre> 





       
        </div> 
        
        
  <!-- .kody pro šablonu -->       		
</div>