<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} 
/**
 * Support Settings
 */ 
?>
<style>
.rtrs-support {
	display: flex; 
	gap: 30px;
	margin-top: 30px;
}
.rtrs-support-box {
	display: table;
}
.rt-document-box + .rt-document-box {
  margin-top: 0;
}
.rt-document-box .rt-box-content p {
	margin: 0 0 15px;
}
.rtrs-support-subhead {
	font-weight: 500;
    color: #444 !important;
    margin-bottom: -5px !important;
}
</style>
<div class="wrap rtrs-support" >  
	<div class="rt-document-box">
		<div class="rt-box-icon"><i class="dashicons dashicons-media-document"></i></div>
		<div class="rt-box-content">
			<h3 class="rt-box-title">How to use Review Schema?</h3>
			<iframe width="611" height="360" src="https://www.youtube.com/embed/P1dYzMcerNs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			
			<h3 class="rt-box-title" style="margin-top: 20px;"><?php esc_html_e( "Online Documentation", "review-schema" ); ?></h3>
			<p>
				<?php _e( "From our online documentation, you will know how to use our pluign. <br> If you face any issue please create a ticket. We will provide you solution as soon as possible.", "review-schema" ); ?> 
			</p> 
			<a class="rt-admin-btn" target="_blank" href="https://www.radiustheme.com/docs/review-schema/review-schema" target="_blank"><?php esc_html_e( "Online documentation", "review-schema" ); ?></a>
			<a class="rt-admin-btn" target="_blank" href="https://www.radiustheme.com/contact/" target="_blank"><?php esc_html_e( "Get Support", "testimonial-slider-showcase" ); ?></a>
		</div>
	</div> 
	
	<?php if ( ! function_exists( 'rtrsp' ) ) { ?>
	<div class="rt-document-box">
		<div class="rt-box-icon"><i class="dashicons dashicons-awards"></i></div>
		<div class="rt-box-content">
			<h3 class="rt-box-title">Pro Features</h3>
			<p class="rtrs-support-subhead">Features Of Review</p>
			<ol style="margin-left: 13px;">
				<li>Unlimited Rating Criteria</li>
				<li>Video Review</li> 
				<li>Support Additional Layouts</li>
				<li>Ajax Pagination Type</li>
				<li>Unlimited Pros Cons</li>
				<li>Social Share</li>
				<li>Review Like Dislike</li>
				<li>Anonymous Review</li>
				<li>Purchase Badge For WC/EDD</li>
				<li>Advanced Layout Styling</li> 
			</ol>
 
			<p class="rtrs-support-subhead">Features Of Schema</p>
			<ol style="margin-left: 13px;">  
				<li>Product Schema</li>
				<li>Course Schema</li>
				<li>Job Posting Schema</li>
				<li>Recipe Schema</li>
				<li>Software App Schema</li>
				<li>Image License Schema</li>
				<li>Special Announcement Schema</li>
			</ol>
			<a href="https://www.radiustheme.com/downloads/wordpress-review-structure-data-schema-plugin/?utm_source=WordPress&utm_medium=reviewschema&utm_campaign=pro_click" class="rt-admin-btn" target="_blank">Get Pro Version</a>
		</div>
	</div>  
	<?php } ?>
</div><!-- /.wrap rtrs-support -->
<?php
/**
 * Support Settings
 */
$options = array();

return apply_filters( 'rtrs_support_settings_options', $options );