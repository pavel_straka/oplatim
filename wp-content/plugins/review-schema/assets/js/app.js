/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/***/ (() => {

(function ($, window) {
  'use strict';

  $(function () {
    function rtrs_repeter(repeter_type) {
      // repeter field 
      var repeter_class = ".rtrs-" + repeter_type;
      $(document).on('click', repeter_class + " .rtrs-field-add", function (e) {
        //check pro 
        var field_length = $(repeter_class + " input").length;
        field_length++;

        if (field_length >= rtrs.pro_cons_limit) {
          $(repeter_class + " .rtrs-field-add").addClass('rtrs-hidden');
        }

        e.preventDefault();
        var new_field = '<div class="rtrs-input-filed"><span class="rtrs-remove-btn">+</span><input type="text" class="form-control" name="rt_' + repeter_type + '[]" placeholder="' + rtrs.write_txt + '"></div>';
        $(repeter_class + " .rtrs-field-add").before(new_field);
      }); // remove repeter field 

      $(document).on('click', repeter_class + " .rtrs-remove-btn", function (e) {
        e.preventDefault();
        $(this).parent().remove();
        var field_length = $(repeter_class + " input").length;
        field_length++;

        if (field_length <= rtrs.pro_cons_limit) {
          $(repeter_class + " .rtrs-field-add").removeClass('rtrs-hidden');
        }
      });
    }

    rtrs_repeter('pros');
    rtrs_repeter('cons'); // google captcha verify  

    if (rtrs.recaptcha_sitekey) {
      $('form.rtrs-form-box').on('submit', function (e) {
        if (!rtrs.recaptcha) return;
        e.preventDefault(); // cache the current form so you make sure to only have data from this one

        var $form = $(this);
        grecaptcha.ready(function () {
          grecaptcha.execute(rtrs.recaptcha_sitekey, {
            action: 'reviewForm'
          }).then(function (token) {
            document.getElementById("gRecaptchaResponse").value = token;
            $form.off('submit').submit();
            $('.rtrs-review-submit').trigger('click');
          });
        });
      });
    } // featherlight popup 


    function rtrs_featherlight_popup() {// $('.rtrs-media-image').featherlight({type: 'image'});  
    }

    rtrs_featherlight_popup(); //edit review 

    $(document).on('click', '.rtrs-review-edit-btn', function (e) {
      e.preventDefault();
      var comment_post_id = $(this).attr('data-comment-post-id');
      var comment_id = $(this).attr('data-comment-id');
      var $this = $(this);
      $.ajax({
        type: "post",
        dataType: "json",
        url: rtrs.ajaxurl,
        data: {
          action: "rtrs_review_edit_form",
          comment_post_id: comment_post_id,
          comment_id: comment_id
        },
        beforeSend: function beforeSend() {
          $this.html('(' + rtrs.loading + ')');
        },
        success: function success(resp) {
          if (resp.success) {
            $this.html('(' + rtrs.edit + ')');
            $('body').prepend(resp.data); //load again video sources

            video_source_option();
            $('#rtrs-video-source').on('change', function () {
              video_source_option();
            });
          } else {
            alert(resp.data);
          }
        }
      });
    }); //edit review 

    $(document).on('click', '.rtrs-review-edit-submit', function (e) {
      e.preventDefault();
      var form = $(this).parents('form').serialize();
      $.ajax({
        type: "post",
        dataType: "json",
        url: rtrs.ajaxurl,
        data: form,
        success: function success(resp) {
          if (resp.success) {
            location.reload();
          } else {
            console.log(resp.data);
          }
        }
      });
    }); //hide review outside click

    $(document).on('mouseup', function (e) {
      var container = $(".rtrs-review-popup");

      if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".rtrs-modal").remove();
      }
    }); // highlight review 

    $(document).on("click", '.rtrs-review-highlight', function (e) {
      var commentID = $(this).data('comment-id');
      var highlight = $(this).attr('data-highlight');

      if (highlight == 'highlight') {
        $(this).html(rtrs.remove_highlight);
        $(this).attr('data-highlight', 'remove'); // highlight bg

        $(this).closest(".rtrs-each-review").addClass('rtrs-top-review');
      } else {
        $(this).html(rtrs.highlight);
        $(this).attr('data-highlight', 'highlight'); // remove highlight

        $(this).closest(".rtrs-each-review").removeClass('rtrs-top-review');
      }

      $.ajax({
        type: "post",
        dataType: "json",
        url: rtrs.ajaxurl,
        data: {
          action: "rtrs_review_hightlight",
          comment_id: commentID,
          highlight: highlight
        },
        beforeSend: function beforeSend() {},
        success: function success(resp) {}
      });
    }); // helpful review  

    $(document).on("click", '.rtrs-review-helpful', function (e) {
      var commentID = $(this).data('comment-id');
      var helpful = $(this).attr('data-helpful');
      var type = $(this).attr('data-type');
      var old_helpful = $(this).find(".helpful-count").html();

      if (helpful == 'helpful') {
        $(this).attr('data-helpful', 'remove');
        old_helpful++;
        $(this).find(".helpful-count").html(old_helpful);
      } else {
        $(this).attr('data-helpful', 'helpful');
        old_helpful--;
        $(this).find(".helpful-count").html(old_helpful);
      } // decrement


      var decrement_selector = type == 'like' ? $(this).next() : $(this).prev();

      if (decrement_selector.attr('data-helpful') == 'remove') {
        decrement_selector.attr('data-helpful', 'helpful');
        var old_decrement_selector = decrement_selector.find(".helpful-count").html();
        old_decrement_selector--;
        decrement_selector.find(".helpful-count").html(old_decrement_selector);
      }

      $.ajax({
        type: "post",
        dataType: "json",
        url: rtrs.ajaxurl,
        data: {
          action: "rtrs_review_helpful",
          comment_id: commentID,
          helpful: helpful,
          type: type,
          nonce: rtrs.nonce
        },
        beforeSend: function beforeSend() {},
        success: function success(resp) {// console.log(resp.data);
        }
      });
    }); // share review  

    $(document).on("click", '.rtrs-share-review', function (e) {
      e.preventDefault();
      var url = $(this).attr('data-url');
      var width = 800;
      var height = 600;
      var top = screen.height / 2 - height / 2;
      var left = screen.width / 2 - width / 2;
      return window.open(url, '', 'location=1,status=1,resizable=yes,width=' + width + ',height=' + height + ',top=' + top + ',left=' + left);
    });
    var url = new URL(window.location.href); // review filter

    $('.rtrs_review_filter').on('change', function (e) {
      var select_value = this.value;
      var data_type = $(this).data('type');

      if (data_type === 'sort') {
        url.searchParams.set('sort_by', select_value);
      } else {
        url.searchParams.set('filter_by', select_value);
      }

      window.history.replaceState(null, null, url);
      var sort_by = url.searchParams.get('sort_by');
      var filter_by = url.searchParams.get('filter_by');
      $.ajax({
        type: "post",
        dataType: "json",
        url: rtrs.ajaxurl,
        data: {
          action: "rtrs_review_filter",
          post_id: rtrs.post_id,
          sort_by: sort_by,
          filter_by: filter_by
        },
        beforeSend: function beforeSend() {
          $('.rtrs-paginate').html(rtrs.loading);
        },
        success: function success(resp) {
          if (resp.success) {
            $('.rtrs-review-list').html(resp.data.review);
            $('.rtrs-paginate').html(resp.data.pagination);
          }

          rtrs_featherlight_popup();
        }
      });
    }); //upload image
    // Todo: select closest id rtrs-image

    $(document).on('click', '#rtrs-upload-box-image', function () {
      $('#rtrs-image').trigger('click');
    });
    $(document).on('change', '#rtrs-image', function (e) {
      var file_data, form_data;
      file_data = $(this).prop('files')[0];
      form_data = new FormData();
      form_data.append('rtrs-image', file_data);
      form_data.append('nonce', rtrs.nonce);
      form_data.append('action', 'rtrs_image_upload');
      $.ajax({
        url: rtrs.ajaxurl,
        type: 'POST',
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function beforeSend() {
          $('.rtrs-image-error').html('');
          $('#rtrs-upload-box-image span').html(rtrs.loading);
        },
        success: function success(resp) {
          if (resp.success) {
            $('.rtrs-preview-imgs').append("<div class='rtrs-preview-img'><img src='" + resp.data.file_info.url + "' /><input type='hidden' name='rt_attachment[imgs][]' value='" + resp.data.file_info.id + "'><span class='rtrs-file-remove' data-id='" + resp.data.file_info.id + "'>x</span></div>");
          } else {
            $('.rtrs-image-error').html(resp.data.msg);
          }

          $('#rtrs-upload-box-image span').html(rtrs.upload_img);
        }
      });
    }); //delete image  

    $(document).on('click', '.rtrs-file-remove', function (e) {
      e.preventDefault();
      var attachment_id = $(this).data('id');

      if (confirm(rtrs.sure_txt)) {
        $(this).parent().remove();
        $.ajax({
          type: "post",
          dataType: "json",
          url: rtrs.ajaxurl,
          data: {
            action: "rtrs_remove_file",
            attachment_id: attachment_id
          },
          success: function success() {}
        });
      }
    }); //upload video

    $(document).on('click', '#rtrs-upload-box-video', function () {
      $('#rtrs-video').trigger('click');
    });
    $(document).on('change', '#rtrs-video', function (e) {
      var file_data, form_data;
      file_data = $(this).prop('files')[0];
      form_data = new FormData();
      form_data.append('rtrs-video', file_data);
      form_data.append('nonce', rtrs.nonce);
      form_data.append('action', 'rtrs_video_upload');
      $.ajax({
        url: rtrs.ajaxurl,
        type: 'POST',
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function beforeSend() {
          $('.rtrs-video-error').html('');
          $('#rtrs-upload-box-video span').html(rtrs.loading);
        },
        success: function success(resp) {
          if (resp.success) {
            $('.rtrs-preview-videos').append("<div class='rtrs-preview-video'><span class='name'>" + resp.data.file_info.name + "</span><input type='hidden' name='rt_attachment[videos][]' value='" + resp.data.file_info.id + "'><span class='rtrs-file-remove'>x</span></div>");
          } else {
            $('.rtrs-video-error').html(resp.data.msg);
          }

          $('#rtrs-upload-box-video span').html(rtrs.upload_video);
        }
      });
    }); // video source select  

    function video_source_option() {
      var video_source = $("#rtrs-video-source").val();

      if (video_source == 'self') {
        $('.rtrs-source-video').show();
        $('.rtrs-source-external').hide();
      } else {
        $('.rtrs-source-video').hide();
        $('.rtrs-source-external').show();
      }
    }

    video_source_option();
    $('#rtrs-video-source').on('change', function () {
      video_source_option();
    }); //self hosted video popup 

    $(document).on('click', '.rtrs-play-self-video', function (e) {
      e.preventDefault();
      var video_url = $(this).attr('data-video-url');
      $.ajax({
        type: "post",
        dataType: "json",
        url: rtrs.ajaxurl,
        data: {
          action: "rtrs_self_video_popup",
          video_url: video_url
        },
        success: function success(resp) {
          if (resp.success) {
            $('body').prepend(resp.data);
          }
        }
      });
    }); //Ajax load more review
    // we will remove the button and load its new copy with AJAX, that's why $('body').on()

    $('body').on('click', '#rtrs-load-more', function () {
      var max_page = $('#rtrs-load-more').attr('data-max');
      var btn = $('#rtrs-load-more');
      var sort_by = url.searchParams.get('sort_by');
      $.ajax({
        url: rtrs.ajaxurl,
        data: {
          action: 'rtrs_pagination',
          post_id: rtrs.post_id,
          current_page: rtrs.current_page,
          sort_by: sort_by
        },
        type: 'POST',
        dataType: "json",
        beforeSend: function beforeSend() {
          btn.text(rtrs.loading);
        },
        success: function success(resp) {
          btn.text('Load More');

          if (resp.success) {
            $('.rtrs-review-list').append(resp.data.review);
          }

          rtrs.current_page++;

          if (rtrs.current_page == max_page) {
            btn.remove();
          }

          rtrs_featherlight_popup();
        }
      });
      return false;
    }); //Ajax pagination with number
    // we will remove the button and load its new copy with AJAX, that's why $('body').on()

    $('body').on('click', '.rtrs-paginate-ajax a', function (e) {
      e.preventDefault();
      var pagi_url = $(this).attr('href');
      var prag_match = pagi_url.match('/comment-page-([0-9]+)/');
      var current_page = 1;

      if (prag_match) {
        current_page = prag_match[1];
      }

      var max_page = $(this).parent().attr('data-max');
      var sort_by = url.searchParams.get('sort_by');
      $.ajax({
        url: rtrs.ajaxurl,
        data: {
          action: 'rtrs_pagination',
          post_id: rtrs.post_id,
          current_page: current_page,
          max_page: max_page,
          pagi_num: true,
          sort_by: sort_by
        },
        type: 'POST',
        dataType: "json",
        beforeSend: function beforeSend() {
          $('.rtrs-paginate-ajax').html(rtrs.loading);
        },
        success: function success(resp) {
          if (resp.success) {
            $('.rtrs-review-list').html(resp.data.review);
            $('.rtrs-paginate-ajax').html(resp.data.pagination); // console.log(resp.data.number);
          }

          rtrs_featherlight_popup();
        }
      });
      return false;
    }); //on scroll pagination

    if ($(".rtrs-paginate-onscroll").length > 0) {
      var onScrollPagi = true;
      $(window).scroll(function () {
        if (!onScrollPagi) return;
        var bottomOffset = 2900; // the distance (in px) from the page bottom when you want to load more posts

        var max_page = $('.rtrs-paginate-onscroll').attr('data-max');
        var sort_by = url.searchParams.get('sort_by');

        if (rtrs.current_page >= max_page) {
          onScrollPagi = false;
          return;
        }

        if ($(document).scrollTop() > $(document).height() - bottomOffset && onScrollPagi == true) {
          $.ajax({
            url: rtrs.ajaxurl,
            data: {
              action: 'rtrs_pagination',
              post_id: rtrs.post_id,
              current_page: rtrs.current_page,
              sort_by: sort_by
            },
            type: 'POST',
            dataType: "json",
            beforeSend: function beforeSend() {
              $('.rtrs-paginate-onscroll').html(rtrs.loading);
              onScrollPagi = false;
            },
            success: function success(resp) {
              if (resp.success) {
                $('.rtrs-review-list').append(resp.data.review);
                $('.rtrs-paginate-onscroll').html('');
                rtrs.current_page++;
                onScrollPagi = true;
                rtrs_featherlight_popup();
              }
            }
          });
        }
      });
    }
  });
})(jQuery, window);

/***/ }),

/***/ "./src/sass/app.scss":
/*!***************************!*\
  !*** ./src/sass/app.scss ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./src/sass/admin.scss":
/*!*****************************!*\
  !*** ./src/sass/admin.scss ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/assets/js/app": 0,
/******/ 			"assets/css/admin": 0,
/******/ 			"assets/css/app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkwp_plugin_rtrs"] = self["webpackChunkwp_plugin_rtrs"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["assets/css/admin","assets/css/app"], () => (__webpack_require__("./src/js/app.js")))
/******/ 	__webpack_require__.O(undefined, ["assets/css/admin","assets/css/app"], () => (__webpack_require__("./src/sass/app.scss")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["assets/css/admin","assets/css/app"], () => (__webpack_require__("./src/sass/admin.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;