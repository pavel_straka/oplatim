;

(function ($) {
  "use strict";

  var isChrome = /Chrome|CriOS/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
  var globalMap = null;
  var geoCoder = new google.maps.Geocoder(); // Map loading

  $(function () {
    // render map cluster
    var map_view = $('.rtcl-map-view');

    if (map_view.length) {
      map_view.each(function () {
        render_map_view($(this));
      });
    } // render single map


    $('.rtcl-map').each(function () {
      render_map($(this));
    });
    startGeoAutoSuggestion();
    getCurrentLocation();
  });

  function getCurrentLocation() {
    $('.rtcl-get-location').on('click', function () {
      var $_item = $(this);
      if ($_item.hasClass('initiated')) return;
      $_item.addClass('initiated');

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          var lat = position.coords.latitude;
          var lng = position.coords.longitude;
          var field_wrap = $_item.parent();
          var address_field = field_wrap.find('.rtcl-geo-address-input');
          field_wrap.find('input.latitude').val(lat);
          field_wrap.find('input.longitude').val(lng); // Get geo address

          if (address_field.length) {
            var latLng = new google.maps.LatLng(lat, lng);
            geoCoder.geocode({
              'latLng': latLng
            }, function (results, status) {
              if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                  var place = results[0];
                  address_field.val(place.formatted_address);

                  if (rtcl_map.location === 'google' && $_item.attr("id") === "rtcl-geo-loc-form") {
                    update_latlng(place.geometry.location);
                  }
                } else {
                  toastr.error("Address not found");
                }
              } else {
                toastr.error("Geocoder failed due to: " + status);
              }
            });
          }
        }, function (error) {
          switch (error.code) {
            case error.PERMISSION_DENIED:
              toastr.error("User denied the request for Geolocation.");
              break;

            case error.POSITION_UNAVAILABLE:
              toastr.error("Location information is unavailable.");
              break;

            case error.TIMEOUT:
              toastr.error("The request to get user location timed out.");
              break;

            case error.UNKNOWN_ERROR:
              toastr.error("An unknown error occurred.");
              break;

            default:
              toastr.error("An unknown error occurred.");
              break;
          }
        });
      } else {
        toastr.error("Geolocation is not supported by this browser.");
      }
    });
  }

  function startGeoAutoSuggestion() {
    $(document).find('.rtcl-geo-address-input').each(function () {
      var _input = $(this);

      this.autocomplete = isChrome ? 'disabled' : 'off';

      var field_wrap = _input.parent();

      var autocomplete = new google.maps.places.Autocomplete(this);
      var div = document.createElement('div');
      div.style.display = 'none';
      document.body.appendChild(div);
      var map = new google.maps.Map(div);
      map = globalMap && _input.hasClass('rtcl_geo_address_input') ? globalMap : map;
      autocomplete.bindTo("bounds", map);
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();

        if (!place.geometry) {
          return;
        }

        _input.val(place.formatted_address);

        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();
        field_wrap.find('input.latitude').val(lat);
        field_wrap.find('input.longitude').val(lng);

        if (_input.hasClass('rtcl_geo_address_input')) {
          map.setCenter(place.geometry.location);
          map.setZoom(rtcl_map.zoom.search || 17);

          if (map.markers && map.markers.length) {
            map.markers[0].setPosition(place.geometry.location);

            if (map.markers[0].iw) {
              map.markers[0].iw.setContent(place.formatted_address);
              map.markers[0].iw.open();
            }
          }

          update_latlng(place.geometry.location);
        }
      });
      google.maps.event.addDomListener(this, 'keydown', function (event) {
        if (event.keyCode === 13 && $('.pac-container:visible').length) {
          event.preventDefault();
        }
      });
    });
  }

  function get_generated_address() {
    var address = [];
    var items = {};
    var address_order = ['address', 'sub_sub_location', 'sub_location', 'location', 'zipcode'];
    $('.rtcl-map-field').map(function () {
      if ($(this).is(":visible")) {
        var type = $(this).getType();
        var attr_name = $(this).attr('name');

        if ((type === 'text' || type === 'textarea') && $(this).val()) {
          items[attr_name] = $(this).val();
        } else if (type === 'select' && $(this).find('option:selected').val()) {
          items[attr_name] = $(this).find('option:selected').text();
        }
      }
    });
    address_order.map(function (value) {
      if (items[value] !== undefined) {
        address.push(items[value]);
      }
    });
    address = address.filter(function (v) {
      return v !== '';
    });
    address = address.join();
    return address;
  }

  function render_map($el) {
    var $markers = $el.find('.marker'),
        map_center_point = new google.maps.LatLng(rtcl_map.center.lat || 0, rtcl_map.center.lng || 0),
        args = {
      zoom: parseInt(rtcl_map.zoom["default"] || 16) || 16,
      center: map_center_point,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoomControl: true,
      scrollwheel: false
    },
        map = new google.maps.Map($el[0], args);
    map.markers = [];
    map.type = $el.data('type');

    if (map.type === "input") {
      globalMap = map;
    } // Add marker


    $markers.each(function () {
      var $marker = $(this),
          latitude = $marker.data('latitude') || map_center_point.lat(),
          longitude = $marker.data('longitude') || map_center_point.lng(),
          address = $marker.data('address') || '',
          position = new google.maps.LatLng(latitude, longitude);
      var marker = new google.maps.Marker({
        map: map,
        position: position,
        draggable: map.type === 'input'
      });
      map.setCenter(position);
      var infoWindow = new google.maps.InfoWindow({
        content: $marker.html()
      });
      google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
      });
      marker.iw = infoWindow;
      map.markers.push(marker);

      if (address) {
        geoCoder.geocode({
          'address': address
        }, function (results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            var place = results[0];
            marker.setPosition(place.geometry.location);
            map.setCenter(place.geometry.location);
            update_latlng(place.geometry.location);
          }
        });
      }

      if (map.type === 'input') {
        google.maps.event.addListener(marker, "dragend", function (event) {
          var point = marker.getPosition();
          map.setCenter(point);
          map.setZoom(rtcl_map.zoom.search || 17);
          update_latlng(point);
          var geo_address_input = $("input.rtcl_geo_address_input");

          if (geo_address_input.length) {
            geoCoder.geocode({
              'latLng': point
            }, function (results, status) {
              if (status === google.maps.GeocoderStatus.OK && results[0]) {
                geo_address_input.val(results[0].formatted_address);
                marker.iw.setContent(results[0].formatted_address);
              }
            });
            var field_wrap = geo_address_input.parent();
            field_wrap.find('input.latitude').val(point.lat());
            field_wrap.find('input.longitude').val(point.lng());
          }
        });

        if (rtcl_map.location === 'local') {
          re_render_map_by_address_change(map);
          $('.rtcl-map-field').on('blur change keyup', function () {
            re_render_map_by_address_change(map);
          });
        }
      }
    }); // center_map(map);
  }

  function re_render_map_by_address_change(map) {
    var address = get_generated_address();
    geoCoder.geocode({
      'address': address
    }, function (results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        var point = results[0].geometry.location,
            marker = map.markers[0];
        marker.setPosition(point);
        map.setCenter(point);
        map.setZoom(rtcl_map.zoom.search || 17);
        update_latlng(point);
      }
    });
  }

  function update_latlng(point) {
    $('#rtcl-latitude').val(point.lat());
    $('#rtcl-longitude').val(point.lng());
  }

  function center_map(map) {
    var bounds = new google.maps.LatLngBounds(); // loop through all markers and create bounds

    $.each(map.markers, function (i, marker) {
      var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
      bounds.extend(latlng);
    }); // only 1 marker?

    if (map.markers.length === 1) {
      map.setCenter(bounds.getCenter());
      map.setZoom(rtcl_map.zoom.search || 17);
    } else {
      map.fitBounds(bounds);
    }
  }

  function render_map_view(view) {
    var bounds = new google.maps.LatLngBounds(),
        mapOptions = {
      center: new google.maps.LatLng(0, 0),
      zoom: 3,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: ''
    },
        infoBox = new InfoBox({
      enableEventPropagation: true,
      maxWidth: 350,
      infoBoxClearance: new google.maps.Size(50, 50),
      alignBottom: true,
      pixelOffset: new google.maps.Size(-47, -75)
    }),
        markers = [],
        addedIDs = [],
        mapType = view.data('map-type') || '';
    var itemData = view.data('map-data') || [];

    if (mapType === 'search') {
      itemData = getMarketData();
    }

    var map = new google.maps.Map(view[0], mapOptions);
    $.each(itemData, function (index, _item) {
      var item = Object.assign({
        id: 0,
        latitude: 0,
        longitude: 0,
        icon: '',
        content: ''
      }, _item);
      var latlong = new google.maps.LatLng(item.latitude, item.longitude);
      var latLongKey = item.latitude + '-' + item.latitude;

      if (addedIDs.indexOf(item.id) === -1) {
        addedIDs.push(item.id);
        bounds.extend(latlong);
        var marker = new google.maps.Marker({
          position: latlong,
          icon: item.icon,
          content: item.content,
          map: map
        });
        markers.push(marker);
        marker.addListener('click', function () {
          infoBox.close();
          infoBox.setContent(marker.content);
          infoBox.setOptions({
            pixelOffset: new google.maps.Size(-47, -75)
          });
          infoBox.open(map, marker);
        });
      }
    });
    var markerCluster = new MarkerClusterer(map, markers, {
      imagePath: rtcl_map.plugin_url + '/assets/images/map/m'
    });
    google.maps.event.addListener(markerCluster, 'click', function (cluster) {
      infoBox.close();
      var markers = cluster.getMarkers();
      var samePosition = true;
      var pos;

      for (var i = 0; i < markers.length; i++) {
        if (!pos) {
          pos = markers[i].position;
        } else if (!pos.equals(markers[i].position)) {
          samePosition = false;
        }
      }

      if (samePosition) {
        var content = '<ul class="list-unstyled info-box-markers-list">';
        $.each(markers, function (index, marker) {
          content += '<li>' + marker.content + '</li>';
        });
        content += '</ul>';
        infoBox.setContent(content);
        infoBox.setOptions({
          pixelOffset: new google.maps.Size(-45, -50)
        });
        infoBox.open(map, markers[markers.length - 1]);
        setTimeout(function () {
          $('.info-box-markers-list').scrollbar();
        }, 50);
        markerCluster.setZoomOnClick(false);
      }
    });
    map.fitBounds(bounds);
  }

  function getMarketData() {
    var data = [];
    $('.rtcl-search-map-lat-long').each(function () {
      var $this = $(this),
          $parent = $this.parents('.rtcl-listing-item'),
          $contentString = $('<div />');
      $contentString.append($parent.find('.rtcl-media').clone());
      $contentString.append($('<h5 class="rtcl-map-item-title" />').append($parent.find('.listing-title a').clone()));
      $contentString.find('h5').wrap('<div class="flex-right"></div>');
      $contentString.find('.flex-right').append('<div class="bottom-rtcl-meta flex-wrap"></div>');
      $contentString.find('.bottom-rtcl-meta').append($parent.find('.rtcl-price-amount').clone());
      $contentString.find('a').addClass('text-overflow').attr('target', '_blank');
      data.push({
        latitude: $this.data('latitude'),
        longitude: $this.data('longitude'),
        id: $this.data('id'),
        icon: $this.data('icon'),
        content: $contentString.html(),
        parent: $parent
      });
    });
    return data;
  }

  function getPostalCode(place) {
    if (place && place.address_components) {
      for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {
          if (place.address_components[i].types[j] === "postal_code") {
            return place.address_components[i].long_name;
          }
        }
      }
    }
  }

  function getMapPlaceData(result, type) {
    var component_name = "";

    if (result.address_components) {
      var _loop = function _loop(i) {
        var component = result.address_components[i];
        $.each(component.types, function (index, value) {
          if (value === type) {
            component_name = component.long_name;
          }
        });
      };

      for (var i = 0; i < result.address_components.length; ++i) {
        _loop(i);
      }
    }

    return component_name;
  }
  /**
   * Unused
   * @param place
   */


  function update_zipcode_address_geco_changed(place) {
    if (!place) {
      return;
    }

    var zipcode_input = $("#rtcl-zipcode");
    var address_input = $("#rtcl-address");

    if (zipcode_input.length) {
      var postal_code = getPostalCode(place);
      zipcode_input.val(postal_code);
    }

    if (address_input.length) {
      var street_address = [getMapPlaceData(place, 'street_number'), getMapPlaceData(place, 'route')].filter(function () {
        return true;
      });
      address_input.val(street_address.join(' '));
    }
  }
})(jQuery);
