<?php

namespace RtclPro\Helpers;

class Installer
{
    public static function activate() {

        if (!is_blog_installed()) {
            return;
        }

        // Check if we are not already running this routine.
        if ('yes' === get_transient('rtcl_pro_installing')) {
            return;
        }

        // If we made it till here nothing is running yet, lets set the transient now.
        set_transient('rtcl_pro_installing', 'yes', MINUTE_IN_SECONDS * 10);

        if (!get_option('rtcl_pro_version')) {
            self::create_options();
        }
        self::create_tables();
        self::update_rtcl_version();

        delete_transient('rtcl_pro_installing');

        do_action('rtcl_flush_rewrite_rules');
        do_action('rtcl_pro_installed');

    }

    private static function create_options() {
        // General settings update
        $isDirty = false;
        $gSettings = get_option('rtcl_general_settings');
        if (!isset($gSettings['compare_limit'])) {
            $gSettings['compare_limit'] = 3;
            $isDirty = true;
        }
        if (!isset($gSettings['default_view'])) {
            $gSettings['default_view'] = 'list';
            $isDirty = true;
        }
        if (!isset($gSettings['location_type'])) {
            $gSettings['location_type'] = 'local';
            $isDirty = true;
        }
        if ($isDirty) {
            update_option('rtcl_general_settings', $gSettings);
        }

        // Moderation settings update
        $isDirty = false;
        $mSettings = get_option('rtcl_moderation_settings');
        if (!isset($mSettings['listing_top_per_page'])) {
            $mSettings['listing_top_per_page'] = 2;
            $isDirty = true;
        }
        if (!isset($mSettings['popular_listing_threshold'])) {
            $mSettings['popular_listing_threshold'] = 1000;
            $isDirty = true;
        }
        if ($isDirty) {
            update_option('rtcl_moderation_settings', $mSettings);
        }

        // Account settings update
        $isDirty = false;
        $aSettings = get_option('rtcl_account_settings');
        if (!isset($aSettings['verify_max_resend_allowed'])) {
            $aSettings['verify_max_resend_allowed'] = 5;
            $isDirty = true;
        }
        if (!isset($aSettings['popular_listing_threshold'])) {
            $aSettings['popular_listing_threshold'] = 1000;
            $isDirty = true;
        }
        if ($isDirty) {
            update_option('rtcl_account_settings', $aSettings);
        }

        // advanced settings update
        $advDirty = false;
        $advSettings = get_option('rtcl_advanced_settings');
        if (!isset($advSettings['myaccount_chat_endpoint'])) {
            $advSettings['myaccount_chat_endpoint'] = 'chat';
            $advDirty = true;
        }
        if (!isset($advSettings['myaccount_verify'])) {
            $advSettings['myaccount_verify'] = 'verify';
            $advDirty = true;
        }
        if ($advDirty) {
            update_option('rtcl_advanced_settings', $advSettings);
        }
    }

    private static function create_tables() {
        global $wpdb;

        $wpdb->hide_errors();

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        dbDelta(self::get_table_schema());
    }

    /**
     * @return array
     */
    static function get_table_schema() {
        global $wpdb;

        $collate = '';

        if ($wpdb->has_cap('collation')) {
            $collate = $wpdb->get_charset_collate();
        }
        $conversation_table_name = $wpdb->prefix . "rtcl_conversations";
        $conversation_message_table_name = $wpdb->prefix . "rtcl_conversation_messages";
        $push_notifications_table_name = $wpdb->prefix . "rtcl_push_notifications";
        $table_schema = [];
        /* We can not use (IF NOT EXISTS) when we use Foreign key REFERENCE declaration inside the table schema
        At this situation will create an sql ERROR : foreign key can't be added with the table because of tablet was not create yet
        SOLUTION 1: First need to check is table is already created or not
        SOLUTION 2: We can first create table except declaring the foreign ke reference and after then we can make the ALTER table using FOREIGN KEY REFERENCE
        */

        if ($wpdb->get_var($wpdb->prepare("SHOW TABLES LIKE %s", $conversation_table_name)) !== $conversation_table_name) {
            $table_schema[] = "CREATE TABLE `{$conversation_table_name}` (
                          `con_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                          `listing_id` bigint(20) unsigned NOT NULL,
                          `sender_id` int(10) unsigned NOT NULL,
                          `recipient_id` int(10) unsigned NOT NULL,
                          `sender_delete` tinyint(1) NOT NULL DEFAULT '0',
                          `recipient_delete` tinyint(1) NOT NULL DEFAULT '0',
                          `last_message_id` int(10) unsigned DEFAULT NULL,
                          `sender_review` tinyint(3) unsigned NOT NULL DEFAULT '0',
                          `recipient_review` tinyint(3) unsigned NOT NULL DEFAULT '0',
                          `invert_review` tinyint(3) unsigned NOT NULL DEFAULT '0',
                          `created_at` timestamp NOT NULL,
                          PRIMARY KEY (`con_id`),
                          KEY `rtcl_conversations_listing_id_index` (`listing_id`),
                          CONSTRAINT `rtcl_conversations_listing_id_foreign` FOREIGN KEY (`listing_id`) REFERENCES `{$wpdb->prefix}posts` (`ID`) ON DELETE CASCADE
                        ) $collate;";
        }
        if ($wpdb->get_var($wpdb->prepare("SHOW TABLES LIKE %s", $conversation_message_table_name)) !== $conversation_message_table_name) {
            $table_schema[] = "CREATE TABLE `{$conversation_message_table_name}` (
                      `message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                      `con_id` bigint(20) unsigned NOT NULL,
                      `source_id` int(10) unsigned NOT NULL,
                      `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                      `is_read` tinyint(1) NOT NULL DEFAULT '0',
                      `created_at` timestamp NOT NULL,
                      PRIMARY KEY (`message_id`),
                      KEY `rtcl_conversation_messages_con_id_index` (`con_id`),
                      CONSTRAINT `rtcl_conversation_messages_con_id_foreign` FOREIGN KEY (`con_id`) REFERENCES `{$wpdb->prefix}rtcl_conversations` (`con_id`) ON DELETE CASCADE
                    ) $collate;";
        }

        if ($wpdb->get_var($wpdb->prepare("SHOW TABLES LIKE %s", $push_notifications_table_name)) !== $push_notifications_table_name) {
            $table_schema[] = "CREATE TABLE `{$push_notifications_table_name}` (
                      `id` BIGINT(20) unsigned NOT NULL AUTO_INCREMENT,
                      `push_token` varchar(255) NOT NULL,
                      `user_id` int(10) unsigned DEFAULT NULL,
                      `events` longtext DEFAULT NULL,
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (`id`),
                      UNIQUE KEY `push_token` (`push_token`)
                      ) $collate;";
        } else {
            // TODO : will remove this after next few version
            if ('2.0.2.5' === get_option('rtcl_pro_version')) {
                $wpdb->query("ALTER TABLE `{$push_notifications_table_name}` RENAME COLUMN `config` TO `events`;");
            }
        }
        return $table_schema;
    }


    private static function update_rtcl_version() {
        delete_option('rtcl_pro_version');
        add_option('rtcl_pro_version', RTCL_PRO_VERSION);
    }

    public static function deactivate() {

    }
}