<?php

namespace RtclPro\Controllers;

use Rtcl\Helpers\Functions;
use Rtcl\Helpers\Link;
use RtclPro\Helpers\Fns;
use RtclPro\Helpers\Options;

class ScriptController
{
    static private $suffix;
    static private $version;
    static private $ajaxurl;

    public static function init() {
        self::$suffix = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';
        self::$version = (defined('WP_DEBUG') && WP_DEBUG) ? time() : RTCL_PRO_VERSION;
        self::$ajaxurl = admin_url('admin-ajax.php');
        if ($current_lang = apply_filters('rtcl_ajaxurl_current_lang', null, self::$ajaxurl)) {
            self::$ajaxurl = add_query_arg('lang', $current_lang, self::$ajaxurl);
        }
        add_filter('rtcl_public_inline_style', [__CLASS__, 'add_public_inline_style'], 10, 2);
        add_action('admin_init', [__CLASS__, 'register_admin_scripts']);
        add_action('admin_enqueue_scripts', [__CLASS__, 'load_admin_script_setting_page']);
        add_action('wp_enqueue_scripts', [__CLASS__, 'register_script']);
    }

    private static function register_script_both_end() {
        $misc_settings = Functions::get_option('rtcl_misc_settings');
        $map_api_key = !empty($misc_settings['map_api_key']) ? sanitize_text_field($misc_settings['map_api_key']) : null;
        if ($map_api_key) {
            $options = Options::google_map_script_options();
            $options['key'] = $map_api_key;
            wp_register_script('rtcl-google-map', add_query_arg($options, 'https://maps.googleapis.com/maps/api/js'));
            wp_register_script('rtcl-map', rtclPro()->get_assets_uri("js/map" . self::$suffix . ".js"), array(
                'jquery',
                'rtcl-google-map'
            ), self::$version, true);
            $center_point = Functions::get_option_item('rtcl_misc_settings', 'map_center');
            $center_point = !empty($center_point) && is_array($center_point) ? wp_parse_args($center_point, ['address' => '', 'lat' => 0, 'lng' => 0]) : ['address' => '', 'lat' => 0, 'lng' => 0];
            wp_localize_script('rtcl-map', 'rtcl_map', apply_filters('rtcl_map_localized_options', [
                    'plugin_url' => RTCL_PRO_URL,
                    'location'   => Fns::location_type(),
                    'center'     => apply_filters('rtcl_map_default_center_latLng', $center_point),
                    'zoom'       => [
                        'default' => !empty($misc_settings['map_zoom_level']) ? absint($misc_settings['map_zoom_level']) : 16,
                        'search'  => 17
                    ]
                ]
            ));
        }
    }

    public static function register_admin_scripts() {
        self::register_script_both_end();
        wp_register_script('rtcl-pro-admin', rtclPro()->get_assets_uri("js/admin.min.js"), ['jquery', 'rtcl-common'], self::$version, true);
        //TODO: will active later when it is necessary
        //wp_register_style('rtcl-pro-admin', rtclPro()->get_assets_uri("css/admin.min.css"));
    }

    public static function register_script() {
        self::register_script_both_end();
        if (wp_script_is('rtcl-google-map', 'registered') && Functions::get_option_item('rtcl_misc_settings', 'map_api_key')) {
            wp_register_script('rtcl-google-markerclusterer', rtclPro()->get_assets_uri("js/markerclusterer.js"));
            wp_register_script('rtcl-google-map-info-box', rtclPro()->get_assets_uri("js/gmap3.infobox.js"), array(
                'rtcl-google-map',
                'rtcl-google-markerclusterer'
            ));
        }
        wp_register_script('photoswipe', rtclPro()->get_assets_uri("vendor/photoswipe/photoswipe.min.js"), '', '4.1.3');
        wp_register_script('photoswipe-ui-default', rtclPro()->get_assets_uri("vendor/photoswipe/photoswipe-ui-default.min.js"), ['photoswipe'], '4.1.3');
        wp_register_script('zoom', rtclPro()->get_assets_uri("vendor/zoom/jquery.zoom.min.js"), ['jquery'], '1.7.21');
        wp_register_style('photoswipe', rtclPro()->get_assets_uri("vendor/photoswipe/photoswipe.css"), '', self::$version);
        wp_register_style('photoswipe-default-skin', rtclPro()->get_assets_uri("vendor/photoswipe/default-skin/default-skin.css"), ['photoswipe'], self::$version);

        $depsScript = ['jquery', 'rtcl-common', 'rtcl-public'];
        if (wp_script_is('rtcl-google-map', 'registered') && Functions::get_option_item('rtcl_moderation_settings', 'has_map', false, 'checkbox')) {
            wp_enqueue_script('rtcl-map');
        }
        wp_register_script('jquery-payment', rtclPro()->get_assets_uri("vendor/jquery.payment.min.js"), ['jquery'], '3.0.0');

        wp_register_script('rtcl-credit-card-form', rtclPro()->get_assets_uri("js/credit-card-form.min.js"), ['jquery-payment', 'rtcl-validator'], self::$version);

        if (Fns::is_enable_chat()) {
            /**
             * If user is logged in send beacon every 15 minutes to set online status
             * If user is logged in then check the Chat notification every 5 second
             */
            wp_register_script('rtcl-beacon', rtclPro()->get_assets_uri("js/beacon.min.js"), ['jquery'], self::$version, true);
            wp_register_script('rtcl-user-chat', rtclPro()->get_assets_uri("js/rtcl-user-chat.min.js"), ['jquery'], self::$version, true);
            wp_register_script('rtcl-chat', rtclPro()->get_assets_uri("js/rtcl-chat" . self::$suffix . ".js"), ['jquery'], self::$version, true);
            $chat_data = [
                'ajaxurl'          => self::$ajaxurl,
                'rest_api_url'     => Link::get_rest_api_url(),
                'date_time_format' => apply_filters('rtcl_chat_date_time_format', 'YYYY, Do MMM h:mm a'),
                'current_user_id'  => get_current_user_id(),
                'lang'             => [
                    'chat_txt'            => esc_html__("Chat", "classified-listing-pro"),
                    'loading'             => esc_html__("Loading ...", "classified-listing-pro"),
                    'confirm'             => esc_html__("Are you sure to delete.", "classified-listing-pro"),
                    'my_chat'             => esc_html__("My Chats", "classified-listing-pro"),
                    'chat_with'           => esc_html__("Chat With", "classified-listing-pro"),
                    'delete_chat'         => esc_html__("Delete chat", "classified-listing-pro"),
                    'select_conversation' => esc_html__("Please select a conversation", "classified-listing-pro"),
                    'no_conversation'     => esc_html__("You have no conversation yet.", "classified-listing-pro"),
                    'message_placeholder' => esc_html__("Type a message here", "classified-listing-pro"),
                    'no_permission'       => esc_html__("No permission to start chat.", "classified-listing-pro"),
                    'server_error'        => esc_html__("Server Error", "classified-listing-pro"),
                ]
            ];
            if (is_user_logged_in()) {
                $depsScript[] = 'rtcl-beacon';
            }

            wp_localize_script('rtcl-chat', 'rtcl_chat', apply_filters('rtcl_localize_chat_data', $chat_data));
            wp_localize_script('rtcl-user-chat', 'rtcl_chat', apply_filters('rtcl_localize_chat_data', $chat_data));
            if (is_singular(rtcl()->post_type)) {
                wp_enqueue_script('rtcl-chat');
            }

            global $wp;

            if (Functions::is_account_page()) {
                if (isset($wp->query_vars['chat'])) {
                    wp_enqueue_script('rtcl-user-chat');
                }
            }
        }
        wp_register_script('rtcl-pro-public', rtclPro()->get_assets_uri("js/public" . self::$suffix . ".js"), $depsScript, self::$version, true);
        wp_register_style('rtcl-pro-public', rtclPro()->get_assets_uri("css/public.min.css"), ['rtcl-public']);

        if (is_singular(rtcl()->post_type)) {
            wp_enqueue_style('photoswipe-default-skin');
            add_action('wp_footer', [__CLASS__, 'photoswipe_placeholder']);
        }

        wp_enqueue_style('rtcl-pro-public');
        wp_enqueue_script('rtcl-pro-public');
    }


    public static function photoswipe_placeholder() {
        Functions::get_template('listing/photoswipe', [], '', rtclPro()->get_plugin_template_path());
    }

    public static function load_admin_script_setting_page() {
        global $pagenow, $post_type;
        // validate page
        if (in_array($pagenow, array('post.php', 'post-new.php', 'edit.php')) && rtcl()->post_type === $post_type) {
            if (wp_script_is('rtcl-google-map', 'registered') && Fns::has_map()) {
                wp_enqueue_script('rtcl-map');
            }
        }

        if (!empty($_GET['post_type']) && $_GET['post_type'] == rtcl()->post_type && !empty($_GET['page']) && $_GET['page'] == 'rtcl-settings') {
            //wp_enqueue_script('rtcl-pro-admin');
            wp_enqueue_style('rtcl-pro-admin');
        }

    }

    public static function add_public_inline_style($style, $options) {

        if (is_array($options) && !empty($options)) {
            // Top
            $top = !empty($options['top']) ? $options['top'] : null;
            if ($top) {
                $style .= ".rtcl .rtcl-badge-_top{ background-color: $top; }";
            }
            $topText = !empty($options['top_text']) ? $options['top_text'] : null;
            if ($topText) {
                $style .= ".rtcl .rtcl-badge-_top{ color: $topText; }";
            }
            // Popular
            $popular = !empty($options['popular']) ? $options['popular'] : null;
            if ($popular) {
                $style .= ".rtcl .rtcl-badge-popular{ background-color: $popular; }";
            }
            $popularText = !empty($options['popular_text']) ? $options['popular_text'] : null;
            if ($popularText) {
                $style .= ".rtcl .rtcl-badge-popular{ color: $popularText; }";
            }

            // Bump Up
            $bump_up = !empty($options['bump_up']) ? $options['bump_up'] : null;
            if ($bump_up) {
                $style .= ".rtcl .rtcl-badge-_bump_up{ background-color: $bump_up; }";
            }
            $bump_upText = !empty($options['bump_up_text']) ? $options['bump_up_text'] : null;
            if ($bump_upText) {
                $style .= ".rtcl .rtcl-badge-_bump_up{ color: $bump_upText; }";
            }
        }

        return $style;
    }

}