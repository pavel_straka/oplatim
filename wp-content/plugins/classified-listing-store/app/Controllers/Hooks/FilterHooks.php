<?php

namespace RtclStore\Controllers\Hooks;

class FilterHooks
{
    public static function init() {
        add_filter('rtcl_addons', [__CLASS__, 'remove_classified_listing_store']);
    }

    public static function remove_classified_listing_store($addons) {
        unset($addons['classified_listing_store']);

        return $addons;
    }

}