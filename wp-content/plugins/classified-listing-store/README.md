### Change log

== 1.4.7 (November 15, 2021)

* Fix installing function not found issue

== 1.4.6.3 (September 14, 2021)

* Review template loading issue
* Fix some coding issues
* Add Addon filter to remove addon if installed

== 1.4.6.2 (September 14, 2021)

* Fix Membership features list
* Fix Membership features list for email
* Fix Membership Plan argument issue
* Update payment receipt pages hooks
* Fix Membership promotion issues